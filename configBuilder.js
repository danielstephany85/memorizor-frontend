const fs = require('fs');
const env = process.env.NODE_ENV;
console.log(env)

const dev = {
    url: "http://localhost:8000"
    //url: "http://192.168.86.250:8000"
};

const prod = {
    url: "https://www.memorizor.com/api"
};

const test = {
    ...dev
}

const config = {
    dev,
    prod,
    test
}

const configString = `
    export default ${JSON.stringify(config[env])};
`

function createFile(filePath, fileContent) {
    fs.writeFile(filePath, fileContent, (e) => {
        if (e) {
            console.log("error creating config.js: " + e)
        } else {
            console.log("config.js created");
        }
    });
}

createFile('./src/config.js', configString);

