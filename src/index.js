import React from 'react';
import ReactDOM from 'react-dom';
import './assets/scss/main.scss';
import Apps from './App';
import registerServiceWorker, { unregister } from './registerServiceWorker';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from "./store/reducers";

//creating the redux store
const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(ReduxThunk)));

ReactDOM.render(
    <Provider store={store} >
        <Apps />
    </Provider>
, document.getElementById('root'));
// registerServiceWorker();
unregister();
