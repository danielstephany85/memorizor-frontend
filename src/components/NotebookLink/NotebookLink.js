import React from 'react';
import { Link } from 'react-router-dom';
import './NotebookLink.scss';
import SetClasses from '@components/_utils/SetClasses';

const NotebookLink = (props) => {
    const { book, className, children, iconAction, ...others} = props,
        baseClass = 'notebook-link',
        classes = SetClasses(baseClass, className);
        
    
    let iconbutton = null

    if (iconAction){
        iconbutton = <button type='button'
            aria-label="delete notebook"
            onClick={iconAction}>
            <i className="fas fa-trash"></i></button>
    }

    return <Link className={classes} {...others}>{children}{iconbutton}</Link>;
}

export default NotebookLink;