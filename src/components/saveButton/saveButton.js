import React, {Component} from 'react';
import './saveButton.scss';
import PropTypes from 'prop-types';

class SaveButton extends Component {
    constructor(props){
        super(props);
        this.state = {
            hasBeenSaved: false
        }
    }

    componentDidUpdate() {
        this.checkforInitalSave();
    }

    checkforInitalSave = () =>{
        if(this.state.hasBeenSaved) return;

        if (this.props.active || this.props.visable){
            this.setState({ hasBeenSaved: true});
        }
    }

    render() {
        let active = this.props.active ? 'save-indicator-button--active' : '';
        let hidden = !this.state.hasBeenSaved ? 'save-indicator-button--hidden' : '';
        let buttonType = this.props.buttonType ? this.props.buttonType : "button";

        return <button className={`save-indicator-button ${hidden} ${active}`} type={buttonType}>{this.props.message}<span className="save-indicator-button__check"></span></button>;
    }
}

SaveButton.propTypes = {
    active: PropTypes.bool,
    buttonType: PropTypes.string,
    message: PropTypes.string.isRequired
}


export default SaveButton;