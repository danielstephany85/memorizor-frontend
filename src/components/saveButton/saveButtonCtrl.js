/*
place new instace of triggerSaveButton in the parent class of the saveButton component;
pass the class instance as the first argument and 
provide the statePropName as the second argument which will be the name of the prop in the parent class that gets passed to the active prop of the saveButton.

triggerSaveButton.clear() in the component will unmount method to prevent state update on removed component
*/

class triggerSaveButton {
    constructor( parent, statePropName, saved = false){
        this._statePropName = statePropName;
        this._saved = saved;
        this._parent = parent;
    } 
    
    call = function() {
        if (typeof this.statePropName === undefined) {
            console.error("ERROR: triggerSaveButton expects \"statePropName\"");
        } else {
            if (this._parent[this._statePropName]) {
                clearTimeout(this._onSaveTimeOut);
                this._parent.setState({ [this._statePropName]: false }, () => {
                    setTimeout(() => {
                        this._parent.setState({ [this._statePropName]: true }, () => {
                            if(this._parent){
                                this._onSaveTimeOut = setTimeout(() => {
                                    this._parent.setState({ [this._statePropName]: false })
                                }, 2000);
                            }
                        });
                    }, 20);
                });
            } else {
                this._parent.setState({ [this._statePropName]: true }, () => {
                    this._onSaveTimeOut = setTimeout(() => {
                        this._parent.setState({ [this._statePropName]: false })
                    }, 2000);
                });
            }
        }
    }

    clear = function() {
            clearTimeout(this._onSaveTimeOut);
    }
    
}

export default triggerSaveButton;