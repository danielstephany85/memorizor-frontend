import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createNote} from '@store/actions/notes';
import Note from "@components/Note";

class NewNoteCard extends Component {

    constructor(props){
        super(props);
        this.state = {
            note: {
                title: this.props.title || '',
                body: this.props.body || '',
                notebookId: this.props.activeNotebook ? this.props.activeNotebook._id : '',
                userId: this.props.userId,
                _id: ''
            }
        }
        this.createNote = bindActionCreators(createNote, this.props.dispatch);
    }

    componentDidMount = () => {
        this.createNote({
            title: this.state.note.title,
            body: this.state.note.body,
            notebookId: this.state.note.notebookId
        })
        .then(json => {
            if(json.data.note){
                this.setState({ note: { ...this.state.note, _id: json.data.note._id }});
            }
        });
    }

    render = () => {
        return <Note isNewNote note={this.state.note}/>;
    }

}

NewNoteCard.propTypes = {
    activeNotebook: PropTypes.object,
    userId: PropTypes.string
}

const mapStateToProps = (state) => (
    {
        activeNotebook: state.userReducer.activeNotebook,
        userId: state.userReducer.user._id,
    }
);

export default connect(mapStateToProps)(NewNoteCard);