import React, {Component} from 'react';
import {Route} from "react-router-dom";

class LoggerRoute extends Component {
    componentDidMount = () => {
        this.props.logRoute();
    }

    componentDidUpdate = () => {
        this.props.logRoute();
    }

    render = () => {
        return (
            <Route {...this.props}/>
        )
    }
}

export default LoggerRoute;