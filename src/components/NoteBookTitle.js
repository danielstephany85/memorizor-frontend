import React from 'react';

const NotebookTitle = (props) => {
    return (
        <h2><i className="fas fa-book-open"></i> {props.title}</h2>
    )
}

export default NotebookTitle;