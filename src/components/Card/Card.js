import React from 'react';
import './Card.scss';
import SetClasses from '@components/_utils/SetClasses';

const Card = (props) => {
    const {className, children, ...others} = props;
    let baseClass = 'card';
    const classes = SetClasses(baseClass, className);

    return <div className={classes} {...others}>{children}</div>;
}

export default Card;