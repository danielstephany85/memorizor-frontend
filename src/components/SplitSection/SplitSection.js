import React from 'react';
import './SplitSection.scss';
import SetClasses from '@components/_utils/SetClasses';

const SplitSection = (props) => {
    const {className, leftSection, rightSection, } = props;
    const BASE_CLASS = 'split-section';
    const classes = SetClasses(BASE_CLASS, className);

    return (
        <section className={classes}>
            <div className="split-section__left">    
                {leftSection}
            </div>
            <div className="split-section__right">
                {rightSection}
            </div>
        </section>
    );
}

export default SplitSection;