import React, { Component } from 'react';
import './MultiSelectPreviewCard.scss';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import SetClasses from '@components/_utils/SetClasses';


class MultiSelectPreviewCard extends Component {

    constructor(props) {
        super(props);
        this.activeDrawer = undefined;
    }

    toggleDrawer = () => (e) => {
        e.preventDefault();
        e.stopPropagation();

        const drawer = this.drawer, 
            button = this.toggleBtn;

        if (drawer.classList.contains('animating')) return;
        drawer.classList.add("animating");
        if (!drawer.classList.contains('active')) {
            //close previously open drawer
            if (this.activeDrawer) {
                let prevDrawer = this.activeDrawer;
                prevDrawer.classList.remove("active");
                setTimeout(() => { 
                    if (!this) return;
                    prevDrawer.classList.remove("display"); 
                }, 300)
            }
            drawer.classList.add("display");
            setTimeout(() => {
                if (!this) return;
                this.trapFocus(drawer, button);
                drawer.classList.add("active");
                drawer.classList.remove("animating");
            }, 50)
            this.activeDrawer = drawer;
        } else {
            drawer.classList.remove("active");
            setTimeout(() => {
                if(!this) return;
                drawer.classList.remove("display");
                drawer.classList.remove("animating");
                this.activeDrawer = undefined;
            }, 300)
        }
    }

    trapFocus = (drawer, btn) => {
        const focusElements = drawer.querySelectorAll('a[href], button, textarea, input[type="text"], input[type="radio"], input[type="checkbox"], select');
        const focusLength = focusElements.length;
        let focusIndex = 0;
        focusElements[0].focus();
        drawer.addEventListener('keydown', function (e) {
            if (e.key === 'Tab' || e.keyCode === 9) {
                e.preventDefault();
                if (!e.shiftKey) {
                    if (focusIndex === focusLength - 1) {
                        focusIndex = 0;
                    } else {
                        focusIndex++;
                    }
                } else {
                    if (focusIndex === 0) {
                        focusIndex = focusLength - 1;
                    } else {
                        focusIndex--;
                    }
                }
                focusElements[focusIndex].focus();
            }
            if (focusIndex === 0 && e.keyCode === 13 && btn) {
                btn.focus();
            }
        });
    }

    multiSelect = () => (e) => {
        e.preventDefault();
        e.stopPropagation();
        const el = this.multiSelectBtn;

        if (!el.classList.contains('active')) {
            el.classList.add('active');
            //send muliselect data to parent element
            this.props.onSelect();
        } else {
            el.classList.remove('active');
            //send muliselect data to parent element
            this.props.onDeselect();
        }
    }



    preventDefault = e => {
        e.preventDefault();
    }
    
    render() {
        const { className, drawerActions, note} = this.props,
            baseClass = 'note-preview-card';
        let classes = SetClasses(baseClass, className);

        if  (this.props.multiSelectActive) {
            classes = SetClasses(baseClass, "multi-select-active");
        }

        const body = note.body.slice(0, 100).replace(/(<([^>]+)>)/ig, "");

        const drawerToggleButton = <button className="drawer-btn" aria-label="toggle note menu" ref={button => { this.toggleBtn = button; }} onClick={this.toggleDrawer()}><i className="fas fa-ellipsis-h"></i></button>
        
        return (
            <Link to={`/note/${this.props.note._id}`} className="note-list__note-container" onClick={this.props.multiSelectActive ? this.multiSelect() : null}>
                <article className={classes} >
                    <header className="note-preview-card__header">
                        <button className="multi-select-btn"
                            aria-label="multi select"
                            ref={(button) => { this.multiSelectBtn = button }}
                            onClick={this.multiSelect()}>
                        </button>
                        {!this.props.multiSelectActive ? drawerToggleButton : null}
                    </header>
                    <div className="note-preview-card__content">
                        <h3>{note.title}</h3>
                        <p dangerouslySetInnerHTML={{ __html: body }}></p>
                    </div>
                    {/* added prevent default to drawer to stop link that card is in from changin page */}
                    <div className="note-preview-card__drawer" ref={(div) => { this.drawer = div }} onClick={this.preventDefault}>
                        <button className="close" aria-label='close' onClick={this.toggleDrawer()}><i className="fas fa-arrow-left"></i></button>
                        {drawerActions}
                    </div>
                </article>
            </Link>
        );
    }
}

MultiSelectPreviewCard.propTypes = {
    onSelect: PropTypes.func.isRequired,
    onDeselect: PropTypes.func.isRequired
}

export default MultiSelectPreviewCard;