import React from 'react';
import './scroll-container.scss';
import SetClasses from '@components/_utils/SetClasses';

const ScrollContainer = (props) => {
    const {className, header, body, children, ...others } = props,
        baseClass = 'scroll-container',
        classes = SetClasses(baseClass, className);

    return (
        <section className={classes} {...others}>
            <div className="static-content">
                {header}
            </div>
            <div className="scroll-content">
                {body ? body : null}
                {children}
            </div>
        </section>
    );
}

export default ScrollContainer;