import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import './listItemLink.scss';
import hasChild from '../_utils/hasChild/hasChild.js';

const ListItemLink = React.forwardRef(function (props, ref) {
    const { children, className, navLink, link, href, to, secondary, activeClassName, exact, strict, ...others } = props;
    let classes = className ? 'list-item-link' + className : 'list-item-link';
    let element;
    let navTo;
    let linkHref;
    let navLinkProps = {};

    if (hasChild(children, "ListItemSecondary")) {
        classes += " list-item-link--with-secondary";
    }
    if (secondary) {
        classes += " list-item-link--secondary";
    }

    if (activeClassName) {
        navLinkProps.activeClassName = activeClassName;
    }
    if (exact) {
        navLinkProps.exact = exact;
    }
    if (strict) {
        navLinkProps.strict = strict;
    }

    if (!to && href) {
        navTo = href;
    } else {
        navTo = to;
    }

    if (!href && to) {
        linkHref = to;
    } else {
        linkHref = href;
    }

    if (navLink) {
        element = (
            <div className={classes} {...others} ref={ref}>
                <NavLink to={navTo} >{children}</NavLink>
            </div>
        );
    } else if (link) {
        element = (
            <div className={classes} {...others} ref={ref}>
                <Link to={navTo} >{children}</Link>
            </div>
        );
    } else {
        element = (
            <div className={classes} {...others} ref={ref}>
                <a href={linkHref} >{children}</a>;
            </div>
        );
    }

    return element;
});

export default ListItemLink;