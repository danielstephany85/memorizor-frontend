export default function SetClasses(baseClass, className) {
    if (typeof baseClass !== 'string' || (typeof className !== 'string' && typeof className !== 'undefined' )){
        console.log('SetClasses expects "baseClass" of type "string and "className" of type "string"');
        return '';
    }
    return className ? baseClass + ' ' + className : baseClass;
}

