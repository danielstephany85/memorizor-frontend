export default function Debounce (debounceTime) {
    this._timeout = undefined;
    
    this.call = function(fn){
        if(typeof this._timeout !== "undefined"){
            clearTimeout(this._timeout);
        }
        this._timeout = setTimeout(()=>{
            return fn();
        }, debounceTime);
    }

    this.clear = function(){
        clearTimeout(this._timeout);
    }
};