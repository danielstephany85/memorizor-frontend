export default function RouteTracker(history) {

    let stepBackLocation = '';
    let locationList = [];

    if (typeof history === "undefined") {
        console.log('ERROR: stepBack requires "history"');
        return;
    }

    const checkAndLogStepForward = () => {
        const pathWithHash = window.location.pathname + window.location.hash;

        if (locationList[locationList.length - 1] !== pathWithHash) {
            locationList.push(pathWithHash);
        }
        return !!(locationList.length - 1);
    }

    const stepBack = (history) => {
        if (typeof history !== "undefined") {
            if (!!(locationList.length - 1)) {
                locationList.pop();
                stepBackLocation = locationList[(locationList.length - 1)];
                // below removes hash when using hashrouter 
                if (stepBackLocation.substring(0,2) === "/#"){
                    stepBackLocation = stepBackLocation.substring(2, stepBackLocation.length);
                }
                history.push(stepBackLocation);
            }
        } else {
            console.log('ERROR: stepBack requires "history"');
        }  
    }

    return {
        locationList: locationList,
        checkAndLogStepForward: checkAndLogStepForward,
        stepBack: stepBack
    }
}