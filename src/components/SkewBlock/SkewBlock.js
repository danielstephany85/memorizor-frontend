import React, {Component} from 'react';
import SetClasses from '@components/_utils/SetClasses';
import ApplyClassName from '@components/_utils/ApplyClassName';
import './SkewBlock.scss';

import PropTypes from 'prop-types';

class SkewBlock extends Component {

    constructor(props){
        super(props);

        this.flip = false;
    }

    componentDidMount(){
        this.observe();
    }

    componentDidUpdate() {
        this.setSkewBlock();
    }

    componentWillUnmount() {
        this.resizeObserver.disconnect();
    }
    
    getSkew(opposite, adjacent) {
        const left = opposite / adjacent;
        return Math.atan(left) * 180 / Math.PI;
    }

    setSkewBlock = () => {
        const blockMidHight = this.skewBlock.clientHeight / 2;
        const skewDegree = this.getSkew(50, blockMidHight);
        const skew = this.flip ? skewDegree.toFixed(2) : skewDegree.toFixed(2) * -1 ;

        this.skewBlock.style.transform = 'skew(' + skew + 'deg)';
    };

    observe = () => {
        const targetNode = this.skewBlock;
        this.resizeObserver = new ResizeObserver(() => {
            this.setSkewBlock();
        });

        this.resizeObserver.observe(targetNode);
    }
    
    render() {
        const { flip, bgColor, className, children, ...others} = this.props,
        BASE_CLASS = 'skew-block',
        CHILD_BASE_CLASS = 'skew-block__background-color',
        BG_COLOR_OPTIONS = ['blue', 'yellow', 'pink'];
        let classes = SetClasses(BASE_CLASS, className);
        classes = flip ? classes += ' skew-block--flip' : classes;
        let childClasses = CHILD_BASE_CLASS;
        childClasses = ApplyClassName(childClasses, CHILD_BASE_CLASS + '--', bgColor, BG_COLOR_OPTIONS);
        
        this.flip = !!flip;

        return (
            <div className={classes} {...others}>
                <div className="skew-block__overflow-container">
                    <div className={childClasses} ref={div => this.skewBlock = div}></div>
                </div>
                <div className="skew-block__content">{children}</div>
            </div>
        );
    }
}

SkewBlock.propTypes = {
    flip: PropTypes.bool,
    bgColor: PropTypes.oneOf(['blue', 'yellow', 'pink'])
}

SkewBlock.defaultPros = {
    bgColor: 'pink'
}

export default SkewBlock;