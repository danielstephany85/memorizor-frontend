import React from 'react';
import './ArticleSection.scss';
import SetClasses from '@components/_utils/SetClasses';

const ArticleSection = (props) => {
    const { className, children, left, right, ...others } = props;
    const BASE_CLASS = 'artile-section';
    let classes = SetClasses(BASE_CLASS, className);

    if(left){
        classes +=  ' ' + BASE_CLASS + '--left';
    } else if (right) {
        classes += ' ' + BASE_CLASS + '--right';
    }

    return (
        <div className={classes} {...others}><div>{children}</div></div>
    );
}

export default ArticleSection;