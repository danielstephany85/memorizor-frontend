import React from 'react';
import { shallow } from 'enzyme';
import Label from './Label';

describe("unit tests for Label component", () => {

    test('that Label mounts without crashing', () => {
        shallow(<Label />);
    });

    it('renders with provided classes', () => {
        const wrapper = shallow(<Label className="test" >test label</Label>);
        expect(wrapper.hasClass('test')).toBe(true);
    });

    it('renders with the label--inline-block class when given the inlineBlock argument', () => {
        const wrapper = shallow(<Label inlineBlock >test label</Label>);
        expect(wrapper.hasClass('label--inline-block')).toBe(true);
    });

    it('renders with children', () => {
        const wrapper = shallow(<Label>test label</Label>);
        expect(wrapper.text()).toEqual('test label');
    });

});