import React from 'react';
import './label.scss';

const Label = React.forwardRef((props, ref) => {
    const {className, inlineBlock,  ...others} = props;
    let classes = className ? 'label ' + className : 'label';

    if (inlineBlock) {
        classes += " label--inline-block";
    }
    
    return <label className={classes} ref={ref} {...others} >{props.children}</label>
});

export default Label;