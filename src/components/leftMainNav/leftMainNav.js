import React from 'react';
import './left-main-nav.scss';
import {NavLink} from 'react-router-dom';
import PropTypes from 'prop-types';

const LeftMainNav = (props) => {

    function triggerStepBack(e) {
        if (e.type === "click"){
            e.preventDefault();
            props.stepBack(props.history);
        }
        if (e.type === "keydown" && e.keyCode === 13){
            e.preventDefault();
            props.stepBack(props.history);
        }
    }

    return(
        <nav className="left-main-nav">
            <div className="left-main-nav__nav-section">
                <div className="back-btn-placeholder">
                    {(props.signedIn && props.canNavigateBack) ? <button onKeyDown={triggerStepBack} onClick={triggerStepBack}><i className="fas fa-arrow-left"></i>back</button> : undefined}
                </div>
                {props.activeNotebook ? <NavLink to={`/notebooks-notes/${props.activeNotebook._id}`} className="current-notebook-link selected"><i className="fas fa-book-open"></i>{props.activeNotebook.title}</NavLink> : null }
                {props.signedIn ? <NavLink to='/notebooks' className="offset-icon"><i className="fas fa-book-open"></i><i className="fas fa-book-open"></i> notebooks</NavLink> : null }
            </div>
            <div className="left-main-nav__nav-section">
                {props.signedIn ? <NavLink to='/trash' ><i className="fas fa-trash"></i> trash</NavLink> : null }
            </div>

            <div className="left-main-nav__mobile-nav">
                <div className="back-btn-placeholder">
                    {(props.signedIn && props.canNavigateBack) ? <button className="nav-icons" onKeyDown={triggerStepBack} onClick={triggerStepBack}><i className="fas fa-arrow-left"></i></button> : undefined}
                </div>
                <div className="back-btn-placeholder">
                    {(props.activeNotebook && props.signedIn ) ? <NavLink to={`/notebooks-notes/${props.activeNotebook._id}`} className="nav-icons selected"><i className="fas fa-book-open"></i></NavLink> : undefined}
                </div>
                {/* <Link to='/all-notes' className="nav-icons" aria-label="All Notes"><i className="fas fa-sticky-note"></i> </Link> */}
                {props.signedIn ? <NavLink to='/notebooks' className="nav-icons nav-icons--offset" aria-label="notebooks"><i className="fas fa-book-open"></i><i className="fas fa-book-open"></i></NavLink> : null }
                {props.signedIn ? <NavLink to='/trash' aria-label="trash" className="nav-icons"><i className="fas fa-trash"></i></NavLink> : null }
            </div>
            
        </nav>
    );
}

LeftMainNav.propTypes = {
    signedIn: PropTypes.bool.isRequired,
    stepBack: PropTypes.func.isRequired,
    activeNotebook: PropTypes.object
}

export default LeftMainNav;