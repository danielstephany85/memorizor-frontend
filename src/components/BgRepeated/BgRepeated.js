import React from 'react';
import SetClasses from '@components/_utils/SetClasses';
import ApplyClassName from '@components/_utils/ApplyClassName';
import PropTypes from 'prop-types';
import './BgRepeated.scss';

const bgColorOptions = ['blue', 'pink'];

const BgRepeated = (props) => {
    const {className, children, bgColor, ...others} = props,
        baseClass = "bg-repeated";
    let classes = SetClasses(baseClass, className);
    classes = ApplyClassName(classes, baseClass + '--', bgColor, bgColorOptions)

    return <div className={classes} {...others}>{children}</div>;
}

BgRepeated.propTypes = {
    bgColor: PropTypes.oneOf(bgColorOptions)
}

export default BgRepeated;