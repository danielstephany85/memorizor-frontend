import React from 'react';
import './input.scss';
import PropTypes from 'prop-types';

const Input = React.forwardRef( function input(props, ref) {
    const {className, borderBottom, ...others} = props;
    let classes = className ? "input " + className : "input";

    if (borderBottom){
        classes += ' input--border-bottom';
    }

    return <input className={classes} {...others} ref={ref} />
}); 

Input.propTypes = {
    borderBottom: PropTypes.bool,
    className: PropTypes.string
}

export default Input;