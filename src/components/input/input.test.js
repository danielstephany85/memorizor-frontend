import React from 'react';
import { shallow } from 'enzyme';
import Input from './Input';

describe("unit tests for Input component", () => {

    test('that Input mounts without crashing', () => {
        shallow(<Input/>);
    });

    it('renders with provided classes', () => {
        const wrapper = shallow(<Input className="test"/>);
        expect(wrapper.hasClass('test')).toBe(true);
    });

    it('renders with "input--border-bottom" class', () => {
        const wrapper = shallow(<Input borderBottom />);
        expect(wrapper.hasClass('input--border-bottom')).toBe(true);
    });

    it('renders with "input--border-bottom" class when className is used', () => {
        const wrapper = shallow(<Input borderBottom className="test"/>);
        expect(wrapper.hasClass('input--border-bottom')).toBe(true);
    });

    it('renders with provided classes when borderBottom is used', () => {
        const wrapper = shallow(<Input className="test" borderBottom />);
        expect(wrapper.hasClass('test')).toBe(true);
    });

});