import React from 'react';
import './listItem.scss';
import hasChild from '../_utils/hasChild/hasChild.js';

const ListItem = React.forwardRef(function(props, ref){
    const {children, className, secondary, button, ...others} = props;
    let listItemType
    let classes = className ? 'list-item ' + className : 'list-item';

    classes = hasChild(children, "ListItemSecondary") ? classes += " list-item--with-secondary" : classes;
    classes = button ? classes += " list-item--with-button" : classes;
    classes = (button && secondary) ? classes += " list-item--secondary" : classes;

    if (button) {
        listItemType = (
            <div role="listitem" className={classes} {...others} ref={ref}>
                <button className="list-item-button">{children}</button>
            </div>
        );
    } else {
        listItemType = <div role="listitem" className={classes} {...others} ref={ref}>{children}</div>;
    }

    return listItemType;
});

export default ListItem;