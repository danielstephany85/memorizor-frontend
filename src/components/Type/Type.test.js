import React from 'react';
import {shallow} from 'enzyme';
import Type from './Type';

describe('Type test suite', () => {
    it('renders without crashing', () => {
        shallow(<Type />);
    });

    it('renders with added classes', () => {
        const wrapper = shallow(<Type className="my-class" />);
        expect(wrapper.hasClass("type my-class")).toBe(true);
    });

    test.each` 
    a       | b    
    ${'h1'} | ${'h1'}
    ${'h2'} | ${'h2'}
    ${'h3'} | ${'h3'}
    ${'h4'} | ${'h4'}
    ${'h5'} | ${'h5'}
    ${'h6'} | ${'h6'}
    ${'p'}  | ${'p'}
    `('renders an $a element when passed $a to element prop', ({a, b}) => {
        const wrapper = shallow(<Type element={a} />);
        expect(wrapper.type()).toBe(b);
    });

    test.each` 
    a       | b    
    ${'h1'} | ${'h1'}
    ${'h2'} | ${'h2'}
    ${'h3'} | ${'h3'}
    ${'h4'} | ${'h4'}
    ${'h5'} | ${'h5'}
    ${'h6'} | ${'h6'}
    ${'p'}  | ${'p'}
    `('renders an $a element when passed $a to element prop', ({ a, b }) => {
        const wrapper = shallow(<Type like={a} />);
        expect(wrapper.hasClass("type--like-" + b)).toBe(true);
    });

});