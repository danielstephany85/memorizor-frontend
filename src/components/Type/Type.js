import React from "react";
import PropTypes from 'prop-types';
import SetClasses from '@components/_utils/SetClasses';
import ApplyClassName from '@components/_utils/ApplyClassName';
import './Type.scss';

const typeOptions = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'title', 'lg-copy', 'body-copy'];

const Type = (props) => {
    const { element, like, className, children, ...others} = props;
    const BASE_CLASS = 'type';
    let classes = SetClasses(BASE_CLASS, className);
    classes += ' type-' + (element ? element : 'p') 
    classes = ApplyClassName(classes, BASE_CLASS + "--like-", like ,typeOptions);
    let Component;

    switch (element) {
        case 'h1': 
            Component = 'h1';
            break;
        case 'h2': 
            Component = 'h2';
            break;
        case 'h3': 
            Component = 'h3';
            break;
        case 'h4': 
            Component = 'h4';
            break;
        case 'h5': 
            Component = 'h5';
            break;
        case 'h6': 
            Component = 'h6';
            break;
        case 'p':
            Component = 'p';
            break;
        default:
        Component = 'p';
    }


    return <Component className={classes} {...others}>{children}</Component>
}

Type.propTypes = {
    element: PropTypes.oneOf(['h1','h2','h3','h4','h5','h6','p']),
    like: PropTypes.oneOf(typeOptions)
}

Type.defaultProps = {
    element: "p",
}

export default Type;