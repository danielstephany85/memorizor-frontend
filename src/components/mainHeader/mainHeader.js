import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import './main-header.scss';

const MainHeader = (props) => {
    return(
        <header className="main-header" >
            <h1 className="main-logo"><Link to='/'>memorizor</Link></h1>
            <nav>
                {!props.signedIn ? <Link to='/login'>login</Link> : null}
                {!props.signedIn ? <Link to='/create-account'>sign-up</Link> : null}
                {props.signedIn ? <Link  to='/settings' className="icon-link" area-label="settins"><i className="fas fa-cog"></i></Link> : null}
            </nav>
        </header>
    );
}

MainHeader.propTypes = {
    signedIn: PropTypes.bool.isRequired,
    logOut: PropTypes.func.isRequired
}

export default MainHeader;