import React from 'react';
import {shallow} from 'enzyme';
import ViewHeader from './ViewHeader';

describe('ViewHeader test suite', () => {

    it('reders without crashing', () => {
        const wrapper = shallow(<ViewHeader></ViewHeader>);
    })

    it('reders with title text', () => {
        const wrapper = shallow(<ViewHeader title="my title"></ViewHeader>);
        expect(wrapper.text()).toBe("my title");
    })

    it('reders with action', () => {
        const wrapper = shallow(<ViewHeader action={<span className="test-action"></span>}></ViewHeader>);
        expect(wrapper.contains(<span className="test-action"></span>)).toBe(true);
    })

    it('reders with title and action', () => {
        const wrapper = shallow(<ViewHeader title="my title" action={<span className="test-action"></span>}></ViewHeader>);
        expect(wrapper.html()).toBe('<header class="view-header">my title<span class="test-action"></span></header>');
    })
});