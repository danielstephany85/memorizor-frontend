import React from 'react';
import PropTypes from 'prop-types';
import './ViewHeader.scss';

const ViewHeader = function(props) {

    return(
        <header className="view-header">
            {props.title}
            {props.action ? props.action:undefined}
        </header>
    );
};

ViewHeader.propTypes = {
    title: PropTypes.node,
}

export default ViewHeader;