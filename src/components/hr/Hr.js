import React from 'react';
import './hr.scss';

const Hr = function hr (props) {
    const {className} = props;
    let classes = className ? "hr-default " + className : "hr-default" 
    return <hr className={classes} />
}

export default Hr;