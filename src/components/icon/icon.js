import React from 'react';
import './Icon.scss';

export default function Icon(props) {

    return(
        <svg className="icon">{props.children}</svg>
    );
}
