import React, {Component} from 'react';

class ModalInterface extends Component {

    componentDidMount(){
        if (this.props.modalOpen){
            setTimeout(()=>{
                this.modal.classList.add("active");
            },1);
        }
    }

    componentDidUpdate = () => {
        if (!this.props.modalOpen){
            this.modal.classList.remove("active");
        }
    }
    
    toggleModalContainerClose = (e) => {
        if (e.target.classList.contains('modal-container')) {
            this.props.closeModal(e);
        }
    }

    render = () => {
        return (
            <div className='modal-container' ref={(div) => { this.modal = div }} onClick={this.toggleModalContainerClose}>
                <div className="modal-interface">
                        {this.props.header ? <header>{this.props.header}</header> : undefined}
                        {this.props.body ? <article>{this.props.body}</article> : undefined}
                        {this.props.children}
                        {this.props.footer ? <footer>{this.props.footer}</footer> : undefined}
                </div>
            </div>
        );
    }
}

export default ModalInterface;