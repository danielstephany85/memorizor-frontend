import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './editor.scss';

import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import EditorToolbar from '@components/EditorToolbar';

class Editor extends Component {

    modules = {
        toolbar: {
            container: "#toolbar",
        }
    }

    formats = [
        "header",
        "font",
        "size",
        "bold",
        "italic",
        "underline",
        "strike",
        "blockquote",
        "list",
        "bullet",
        "indent",
        "align",
        "link",
        "image",
        "color",
        "code-block",
        "background"
    ];

    handleChange = (html) => {
        this.setState({ editorHtml: html });
    }

    render = () => {
        const { editorTitle, noteTop, className, saveBtn, ...others} = this.props,
            baseClass = "text-editor",
            classes = className ? baseClass + ' ' + className : baseClass;
        
        return(
            <div className={classes}> 
                <EditorToolbar saveBtn={saveBtn} />
                {/* <div className="text-editor__top">
                    {noteTop}
                </div> */}
                <div className="text-editor__main-content">
                    <div className="text-editor__title-container">
                        {editorTitle}
                    </div>
                    <ReactQuill
                        formats={this.formats}
                        modules={this.modules}
                        theme={"snow"}
                        {...others}
                    />
                </div>
            </div>
        );
    }

}


Editor.propTypes = {
    handleChange: PropTypes.func,
}

export default Editor;