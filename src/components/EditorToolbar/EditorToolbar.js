import React from 'react';
import './editorToolbar.scss';

export default function CustomToolbar(props) {
    return (
        <div id="toolbar" className="mtoolbar">
            <select className="ql-header" defaultValue={""} onChange={e => e.persist()}>
                <option value="1"></option>
                <option value="2"></option>
                <option value="3"></option>
                <option value=""></option>
            </select>
            <select className="ql-align" defaultValue={""} onChange={e => e.persist()}>
                <option value="center"></option>
                <option value="right"></option>
                <option value=""></option>
            </select>
            <button className="ql-bold"></button>
            <button className="ql-italic"></button>
            <button className="ql-underline"></button>
            <button className="ql-strike"></button>
            <button className="ql-list" value="ordered"></button>
            <button className="ql-list" value="bullet"></button>
            <button className="ql-blockquote"></button>
            <button className="ql-code-block"></button>
            <select className="ql-color">
                <option value="red"></option>
                <option value="green"></option>
                <option value="blue"></option>
                <option value="orange"></option>
                <option value="violet"></option>
                <option value="#d0d1d2"></option>
                <option value=""></option>
            </select>
            <select className="ql-background">
                <option value="red"></option>
                <option value="green"></option>
                <option value="blue"></option>
                <option value="orange"></option>
                <option value="violet"></option>
                <option value="#d0d1d2"></option>
                <option value=""></option>
            </select>
            <button className="ql-link"></button>
            {props.saveBtn}
        </div>
    )
}