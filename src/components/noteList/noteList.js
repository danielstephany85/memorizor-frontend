import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import NotePreviewCard from '@components/NotePreviewCard'; 

class NoteList extends Component  {

    render = () => {
        let notes;
        if (this.props.notes.length) {
            notes = this.props.notes.map((note, i) => {
                return <Link to={`/note/${note._id}`} className="note-list__note-container" key={note._id}>
                    <NotePreviewCard note={note} moveToTrash={this.props.moveToTrash}/>
                </Link>;
            });
        } else {
            notes = <span>no notes curently exist</span>
        }
        return (
            <div className="note-list">
                <div className="note-list__row">
                    {notes}
                </div>
            </div>
        );
    }
}

NoteList.propTypes = {
    notes: PropTypes.array,
    moveToTrash: PropTypes.func
}

export default NoteList;