import React from 'react';
import { shallow } from 'enzyme';
import NoteList from './noteList';

const notes = [
    {title: "test", body: "test"},
    { title: "test2", body: "test2" }
];

let wrapper;

it('renders without crashing', () => {
    wrapper = shallow(<NoteList notes={notes}/>);
});

it('renders 2 note containers', ()=> {
    expect(wrapper.find('.note-list__note-container').length).toBe(2);
});