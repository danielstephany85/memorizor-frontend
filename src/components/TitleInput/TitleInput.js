import React from 'react';
import SetClasses from '@components/_utils/SetClasses';
import './titleInput.scss';

const TitleInput = React.forwardRef(function titleInput(props, ref) {
    const {className, ...others} = props;
    const classes = SetClasses('mTitle-input', className);

    return <input className={classes} ref={ref} {...others}/>
});

export default TitleInput;