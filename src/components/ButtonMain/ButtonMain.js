import React from 'react';
import PropTypes from 'prop-types';
// import Icon from '../icon/icon.js';
import './ButtonMain.scss';
import SetClasses from '@components/_utils/SetClasses';
import ApplyClassName from '@components/_utils/ApplyClassName';

const ButtonMain = React.forwardRef(function buttonMain(props, ref) {
    const { className, children, bgColor, size, component, iconStart, iconEnd, loading, ...others} = props;
    const Component = component;
    const loadSpan = loading ? <span className="button-main__loading" /> : null;
    const baseClass = 'button-main';
    let colorOptions = ['primary', 'primary-light', 'secondary', 'secondary-light'];
    let sizeOptions = ['sm', 'md', 'lg'];
    let classes = SetClasses(baseClass, className);
    classes = ApplyClassName(classes, baseClass + '--bg-', bgColor, colorOptions);
    classes = ApplyClassName(classes, baseClass + '--size-', size, sizeOptions);

    if (iconStart) { 
        classes += ' ' + baseClass + '--icon-start';
    } else if (iconEnd) {
        classes += ' ' + baseClass + '--icon-end';
    }

    if (loading) {
        classes += " button-main--loading"
    }
    
    if (component){
        return <Component className={classes} ref={ref} {...others}>{children}{loadSpan}</Component>
    }   

    return <button className={classes} ref={ref} {...others}>{children}{loadSpan}</button>;
});

ButtonMain.propTypes = {
    bgColor: PropTypes.oneOf(['primary', 'primary-light', 'secondary', 'secondary-light']),
    size: PropTypes.oneOf(['sm', 'md', 'lg']),
    component: PropTypes.object
}

ButtonMain.defaultProps = {
    size: 'md',
};

export default ButtonMain;