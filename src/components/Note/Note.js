import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import './Note.scss';
import { updateNote, deleteNote, moveToTrash } from '@store/actions/notes';
import NoteHeader from '@components/NoteHeader';
import Debounce from '@components/_utils/Debounce';
import SaveButton from '@components/saveButton/saveButton.js';
import TriggerSaveButton from '@components/saveButton/saveButtonCtrl';
import Editor from '@components/Editor';
import TitleInput from '@components/TitleInput';

class Note extends Component {

    constructor(props){
        super(props);
        this.state = {
            title: props.note.title,
            body: props.note.body,
        }

        this.debounce = new Debounce(650);
        this.moveToTrash = bindActionCreators(moveToTrash, this.props.dispatch);
        this.updateNote = bindActionCreators(updateNote, this.props.dispatch);
        this.deleteNote = bindActionCreators(deleteNote, this.props.dispatch);
        this.TriggerSaveButton = new TriggerSaveButton(this, "saved");
        this.canSave = true;
    }
    
    componentDidMount() {
        this.title.focus();
    }

    componentWillUnmount() {
        if (!this.state.title.trim() && (!this.state.body.trim() || this.state.body.trim() === "<p><br></p>")) {
            this.deleteNote({ _id: this.props.note._id });
        } else if (this.props.note._id && this.canSave){
            this.updateNote({
                note: {
                    ...this.props.note,
                    title: this.state.title,
                    body: this.state.body,
                }
            });
        }
        this.debounce.clear();
        this.TriggerSaveButton.clear();
    }

    submitUpdate = (e) => {
        if (typeof e !== "undefined") {
            e.preventDefault();
        }
        if (this.props.note._id && this.canSave){
            this.updateNote({
                note: {
                    ...this.props.note,
                    title: this.state.title,
                    body: this.state.body,
                }
            }).then((json) => {
                if (json.status === 'success') this.TriggerSaveButton.call();
            });
        }
    }

    updateBodyValue = (value) => {
        this.setState({ body: value }, () => {
            this.debounce.call(() => { this.submitUpdate() });
        });
    }

    updateTitleValue = (e) => {
        this.setState({ title: e.target.value }, () => {
            this.debounce.call(() => { this.submitUpdate() });
        });
    }

    handleMoveToTrash = () => {
        const note = {
                        ...this.props.note,
                        title: this.state.title,
                        body: this.state.body,
                        in_trash: false,
                    };

        this.moveToTrash(note)
            .then((json) => {
                this.canSave = false;
                if (json.status === 'success') return this.props.history.goBack();
            });
    }

    handleRestoreNote = () => {
        const data = {
            note: {
                ...this.props.note,
                title: this.state.title,
                body: this.state.body,
                in_trash: false,
            }
        }
        this.updateNote(data)
            .then((json) => {
                this.canSave = false;
                if (json.status === 'success') return this.props.history.goBack();
            });
    }

    handleDeleteNote = () => {
        this.deleteNote(this.props.note)
            .then((json) => {
                this.canSave = false;
                if (json.status === 'success') return this.props.history.goBack();
            });
    }

    render = () => {
        const title = (<TitleInput type="text" name="title" value={this.state.title} placeholder="title" ref={(input) => { this.title = input }} onChange={this.updateTitleValue} />);
        const saveBtn = (<SaveButton active={this.state.saved} message="saved" buttonType="submit" />);


        return (
            <div className="note">
                <NoteHeader isNewNote={this.props.isNewNote} 
                    moveToTrash={this.handleMoveToTrash}
                    restoreNote={this.handleRestoreNote}
                    deleteNote={this.handleDeleteNote}
                    note={this.props.note} 
                    history={this.props.history}/>
                <div className="note__content">
                    <form onSubmit={(e) => this.submitUpdate(e)} className="form-content">
                        <Editor value={this.state.body} 
                            onChange={this.updateBodyValue} 
                            placeholder="note content..."
                            editorTitle={title}
                            // noteTop={saveBtn}
                            saveBtn={saveBtn}
                        /> 
                    </form>
                </div>
            </div>
        );
    }
}

Note.propTypes = {
    note: PropTypes.object.isRequired,
    isNewNote: PropTypes.bool
}

Note.defaultProps = {
    isNewNote: false
}

export default connect()(Note);