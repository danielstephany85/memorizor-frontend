import React from 'react';
import { shallow } from 'enzyme';
import NotePreviewCard from './notePreviewCard';

const notes = [
    { title: "test", body: "test" },
    { title: "test2", body: "test2" }
];

let wrapper;

it('renders without crashing', () => {
    wrapper = shallow(<NotePreviewCard title={'test title'} body={"test body"}/>);
});

it('renders with title text', () => {
    expect(wrapper.find('h3').text()).toEqual('test title');
});

it('renders with body text', () => {
    expect(wrapper.find('p').text()).toEqual('test body');
});