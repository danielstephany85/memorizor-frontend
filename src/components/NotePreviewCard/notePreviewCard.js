import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './NotePreviewCard.scss';

class NotePreviewCard extends Component {

    constructor(props){
        super(props);
        this.state = {
            activeDrawer: false,
            activeMultiSelect: false,
        }
    }

    activeDrawer = undefined;

    toggleDrawer = (e) => {
        e.preventDefault();
        const drawer = this.drawer;
        if (drawer.classList.contains('animating')) return;
        drawer.classList.add("animating");
        if (!drawer.classList.contains('active')) {
            //close previously open drawer
            if (this.activeDrawer) {
                let prevDrawer = this.activeDrawer;
                prevDrawer.classList.remove("active");
                setTimeout(() => { prevDrawer.classList.remove("display"); }, 300)
            }
            drawer.classList.add("display");
            setTimeout(() => { 
                this.trapFocus(drawer, this.toggleBtn);
                drawer.classList.add("active"); 
                drawer.classList.remove("animating");
            }, 50)
            this.activeDrawer = drawer;
        } else {
            drawer.classList.remove("active");
            setTimeout(() => { 
                drawer.classList.remove("display");
                drawer.classList.remove("animating");
                this.activeDrawer = undefined;
            }, 300)
        }
    }

    trapFocus = (drawer, btn) => {
        const focusElements = drawer.querySelectorAll('a[href], button, textarea, input[type="text"], input[type="radio"], input[type="checkbox"], select');
        const focusLength = focusElements.length;
        let focusIndex = 0;
        focusElements[0].focus();
        drawer.addEventListener('keydown', function (e) {
            if (e.key === 'Tab' || e.keyCode === 9) {
                e.preventDefault();
                if (!e.shiftKey) {
                    if (focusIndex === focusLength - 1) {
                        focusIndex = 0;
                    } else {
                        focusIndex++;
                    }
                } else {
                    if (focusIndex === 0) {
                        focusIndex = focusLength - 1;
                    } else {
                        focusIndex--;
                    }
                }
                focusElements[focusIndex].focus();
            }
            if (focusElements[focusIndex].classList.contains('close') && e.keyCode === 13 && btn) {
                btn.focus();
            }
        });
    }

    render = () => {

        return (
                <article className="note-preview-card" >
                    <header className="note-preview-card__header">
                        <button className="drawer-btn" aria-label="toggle note menu" ref={(button) => {this.toggleBtn = button}} onClick={(e) => { this.toggleDrawer(e) }}><i className="fas fa-ellipsis-h"></i></button>
                    </header>
                    <div className="note-preview-card__content">
                        <h3>{this.props.note.title}</h3>
                        <p>{this.props.note.body}</p>
                    </div>
                    {/* added prevent default to drawer to stop link that card is in from changin page */}
                    <div className="note-preview-card__drawer" ref={(div) => { this.drawer = div }} onClick={(e)=>{e.preventDefault()}}>
                        <button className="close" aria-label='close' onClick={(e) => { this.toggleDrawer(e) }}><i className="fas fa-arrow-left"></i></button>
                        <button className="drawer-item" onClick={()=>{this.props.moveToTrash(this.props.note)}}>place in trash</button>
                    </div>
                </article>
            )
        ;
    }
}



NotePreviewCard.propTypes = {
    note: PropTypes.object,
    moveToTrash: PropTypes.func
}

export default NotePreviewCard;