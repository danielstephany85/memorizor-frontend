import React from 'react';
import PropTypes from 'prop-types';
import ButtonMain from '@components/ButtonMain';

const NoteHeader = (props) => {
    const { isNewNote, note, moveToTrash, restoreNote, deleteNote} = props;

    return (
        <header>
            <div className="note-header-buttons">
                {(!isNewNote && !note.in_trash) ? <ButtonMain onClick={moveToTrash}>Place in trash</ButtonMain> : ""}
                {note.in_trash ? <ButtonMain onClick={restoreNote}>Restore note</ButtonMain> : ""}
                {note.in_trash ? <ButtonMain onClick={deleteNote}>Delete note</ButtonMain> : ""}
            </div>
        </header>
    );
}

NoteHeader.propTypes = {
    note: PropTypes.object,
    history: PropTypes.object,
    moveToTrash: PropTypes.func.isRequired,
    restoreNote: PropTypes.func.isRequired,
    deleteNote: PropTypes.func.isRequired
}

export default NoteHeader;