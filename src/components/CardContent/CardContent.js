import React from 'react';
import './CardContent.scss';
import SetClasses from '@components/_utils/SetClasses';

const CardContent = (props) => {
    const {className, children, ...others} = props;
    let baseClass = 'card-content';
    const classes = SetClasses(baseClass, className);

    return <div className={classes} {...others}>{children}</div>;
}

export default CardContent;