let initialState = {
    signedIn: false,
    user: undefined,
    token: undefined,
    activeNotebook: undefined
}

export default function userReducer(state = initialState, action) {
    let updatedNotebookList,
        activeNotebook;

    switch (action.type) {
        case "SET_USER":
            return {
                ...state,
                signedIn: true,
                user: action.user,
                token: action.token
            };
        case "UPDATE_USER":
            return {
                ...state,
                user: action.user
            }
        case "LOGOUT":
            return {
                ...state,
                signedIn: false,
                user: undefined,
                notes: undefined,
                token: undefined,
                activeNotebook: undefined
            };
        case "UPDATE_NOTEBOOKS":
            return {
                ...state,
                user: {
                    ...state.user,
                    notebooks: action.notebooks
                }
            }
        case "SET_ACTIVE_NOTEBOOK":
            if (action.notebookId !== undefined && action.notebookTitle !== undefined) {
                activeNotebook = {
                    _id: action.notebookId,
                    title: action.notebookTitle
                }
            } else {
                activeNotebook = undefined;
            }
            return {
                ...state,
                activeNotebook: activeNotebook
            }
        case "_ADD_NOTEBOOK_TO_TRASH":
            updatedNotebookList = state.user.notebooks;
            updatedNotebookList.forEach( notebook => {
                if (notebook._id === action.notebookId){
                    notebook.in_trash = true;
                }
            });
            return {
                ...state,
                user: {
                    ...state.user,
                    notebooks: updatedNotebookList
                }
            }
        case "_RESTORE_NOTEBOOK": 
            updatedNotebookList = state.user.notebooks
            updatedNotebookList.forEach((book)=>{
                if (book._id === action.notebookId){
                    book.in_trash = false;
                }
            });
            return {
                ...state,
                user: {
                    ...state.user,
                    notebooks: updatedNotebookList
                }
            }
        case "DELETE_NOTEBOOK":
            return {
                ...state,
                user: {
                    ...state.user,
                    notebooks: state.user.notebooks.filter( book => (book._id !== action.notebookId))
                }
            }
        default:
            return state;
    }
}