let initialState = {
    notes: [],
    trashNotes: []
}

export default function notesReducer(state = initialState, action) {
    switch (action.type) {
        case "_SET_NOTES":
            return {
                ...state,
                notes: action.notes
            };
        case "ADD_NOTE":
            if(state.notes === undefined) state.notes = [];
            state.notes.push(action.note)
            return {
                ...state,
                //ADD NOTE
            };
        case "_SET_TRASH_NOTES":
            return {
                ...state,
                trashNotes: action.notes
            }
        case "_MOVE_TO_TRASH":
            return {
                ...state,
                activeNotebookNotes: state.activeNotebookNotes.filter(note => note._id !== action.note._id),
                trashNotes: [...state.trashNotes, action.note]
            };
        case "ADD_NOTES_TO_TRASH":
            let newTrashNotes = [];
            state.notes.forEach(note => {
                if (note.notebookId === action.notebookId){
                    note.in_trash = true;
                    newTrashNotes.push({...note});
                }
            });

            return {
                ...state,
                notes: state.notes.filter(note => note.notebookId !== action.notebookId),
                trashNotes: [...state.trashNotes, ...newTrashNotes]
            }
        case "_DELETE_NOTE":
            return {
                ...state,
                notes: state.notes.filter(note => note._id !== action.note._id),
                trashNotes: state.trashNotes.filter(note => note._id !== action.note._id)
                //REMOVE NOTE
            };
        case "_RESTORE_NOTE":
            return {
                notes: [...state.notes, action.note],
                trashNotes: state.trashNotes.filter(note => note._id !== action.note._id)
            }
        case "_DELETE_SELECTED_NOTES":
            return {
                ...state,
                trashNotes: state.trashNotes.filter(note => {
                    let deletedNotes = action.notes;
                    let i = 0;
                    for (i; i < deletedNotes.length; i++){
                        if (deletedNotes[i] === note._id){
                            return false;
                        }
                    }
                    return true;
                })
            };
        case "_RESTORE_SELECTED_NOTES":
            const notesToRestore = state.trashNotes.filter(trashNote => {
                let noteMatch = false;
                action.notes.forEach(noteID => {
                    if (trashNote._id === noteID) {
                        noteMatch = true;
                        trashNote.in_trash = false;
                        trashNote.notebookId = action.notebookId;
                    }
                });
                return noteMatch;
            });
            
            const updatedTrashNotes = state.trashNotes.filter(note => {
                let deletedNotesIdArray = action.notes;
                let i = 0;
                for (i; i < deletedNotesIdArray.length; i++) {
                    if (deletedNotesIdArray[i] === note._id) {
                        return false;
                    }
                }
                return true;
            })

            return {
                ...state,
                notes: [
                    ...state.notes,
                    ...notesToRestore
                ],
                trashNotes: updatedTrashNotes
            };
        case "_UPDATE_NOTE":
            return {
                ...state,
                notes: state.notes.map((note)=> {
                    if(note._id === action.note._id) return action.note;
                    return note;
                })
            };
        case "_RESTORE_NOTEBOOK_NOTES":
            let restoredNotes = [];
            let remainingTrashNotes = state.trashNotes.filter((note)=> {
                if (note.notebookId === action.notebookId){
                    restoredNotes.push(note);
                    return false;
                }
                return true;
            });
            return {
                ...state,
                notes: [...state.notes, ...restoredNotes],
                trashNotes: remainingTrashNotes
             };
        case "DELETE_NOTEBOOK_NOTES":
            return {
                ...state,
                trashNotes: state.trashNotes.filter( note => (note.notebookId !== action.notebookId))
            }
        case '_SET_ACTIVE_NOTEBOOK_LIST':
            return {
                ...state,
                activeNotebookNotes: action.activeNotebookNotes
            }
        case '_UPDATE_ACTIVE_NOTEBOOK_LIST':
            const activeNotebooks = state.activeNotebookNotes.filter( note => {
                let keepNote = true;
                action.notes.forEach( noteToRemove => {
                    if (note._id === noteToRemove._id){
                        keepNote = false
                    }
                });
                return keepNote;
            });
            return {
                ...state,
                activeNotebookNotes: activeNotebooks
            }
        default:
            return state;
    }
}