import config from '../../config';


export function _setUser(user, token) {
    return {
        type: "SET_USER",
        user: user,
        token: token
    }
}

export function _updateUser(user) {
    return {
        type: "UPDATE_USER",
        user: user
    }
}


export function logOut(){
    return{
        type: "LOGOUT"
    };
}

export function updateNotebooks(notebooks){
    return {
        type: 'UPDATE_NOTEBOOKS',
        notebooks: notebooks
    }
}


export function setActiveNotebook(notebookId = undefined, notebookTitle = undefined) {
    return {
        type: 'SET_ACTIVE_NOTEBOOK',
        notebookId: notebookId,
        notebookTitle: notebookTitle
    }
}

export function _addNotebookToTrash(notebookId) {
    return {
        type: '_ADD_NOTEBOOK_TO_TRASH',
        notebookId: notebookId
    }
}


export function addNotesToTrash(notebookId) {
    return {
        type: 'ADD_NOTES_TO_TRASH',
        notebookId: notebookId
    }
}

export function _restoreNotebookNotes(notebookId) {
    return {
        type: "_RESTORE_NOTEBOOK_NOTES",
        notebookId: notebookId
    }
}

export function _restoreNotebook(notebookId) {
    return {
        type: "_RESTORE_NOTEBOOK",
        notebookId: notebookId
    }
}

export function _deleteNotebook(notebookId) {
    return {
        type: "DELETE_NOTEBOOK",
        notebookId: notebookId
    }
}

export function _deleteNotebookNotes(notebookId) {
    return {
        type: "DELETE_NOTEBOOK_NOTES",
        notebookId: notebookId
    }
}

export function creatUser(data){
    return dispatch => {
        return new Promise((resolve, reject) => {
            fetch(`${config.url}/create-user`, {
                method: "POST",
                mode: "cors",
                headers: { "Content-Type": "application/json; charset=utf-8" },
                body: JSON.stringify(data)
            })
            .then(res => res.json())
            .then((json) => {
                if (json.status === 'error') {
                    return reject(json);
                } else {
                    dispatch(_setUser(json.data.user, json.data.token));
                    return resolve(json);
                }
            }).catch(e => {
                console.log(e);
            });
        });
    }
}

export function loginUser(data){
    return dispatch => {
        return new Promise((resolve, reject) => {
            fetch(`${config.url}/authenticate-user`, {
                method: "POST",
                mode: "cors",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(data)
            })
                .then(res => res.json())
                .then((json) => {
                    if (json.status === "success") {
                        dispatch(_setUser(json.data.user, json.data.token));
                        return resolve(json);
                    } else {
                        return reject(json);
                    }
                })
                .catch((e) => {
                    console.log(e);
                });
        });
    }
}

export function setUser(token){
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            if (token) {
                fetch(`${config.url}/user`, {
                    headers: { 'x-access-token': token }
                })
                .then(res => res.json())
                .then(json => {
                    if (json.status === 'success') {
                        dispatch(_setUser(json.data.user, token));
                        return resolve(json);
                    }
                })
                .catch((e) => {
                    return reject(e);
                });
            }
        })
    }
}

export function updateUser(userData) {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            fetch(`${config.url}/user`, {
                method: 'PUT',
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    'x-access-token': getState().userReducer.token
                },
                body: JSON.stringify(userData)
            }).then(response => response.json())
            .then(json => {
                dispatch(_updateUser(json.data.user));
                return resolve(json);
            })
            .catch(e => {
                return reject(e);
            });
        });
    }
}

export function updatePassword(data) {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            fetch(`${config.url}/user/update-password`, {
                method: 'Put',
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    'x-access-token': getState().userReducer.token
                },
                body: JSON.stringify(data)
            })
            .then(res => res.json())
            .then((json) => {
                return resolve(json);
            })
            .catch(e => {
                reject(e);
            });
        });
    }
}

export function addNotebook(title){
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            const data = {
                title: title
            }
            fetch(`${config.url}/notebooks`, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    'x-access-token': getState().userReducer.token
                },
                body: JSON.stringify(data)
            })
                .then(res => res.json())
                .then(json => {
                    if(json.status === "success"){
                        dispatch(updateNotebooks(json.data.user.notebooks));
                        return resolve(json);
                    }
                    reject(json)
                })
                .catch(e => {
                    reject(e);
                });
        });
    }
}

export function addNotebookToTrash(notebookId) {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            let data = { notebookId: notebookId }
            fetch(`${config.url}/notebooks/trash`, {
                method: "POST",
                mode: "cors",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    'x-access-token': getState().userReducer.token
                },
                body: JSON.stringify(data)
            })
                .then(res => res.json())
                .then(json => {
                    if (json.status === "success") {
                        dispatch( _addNotebookToTrash(notebookId) )
                        dispatch( addNotesToTrash(notebookId) );
                        //check if this was the active notebook and if so unset it
                        if (getState().userReducer.activeNotebook && getState().userReducer.activeNotebook._id === notebookId) {
                            dispatch(setActiveNotebook(undefined, undefined));
                        }
                        return resolve();
                    }
                })
                .catch(e => {
                    console.log(e);
                    return reject();
                });
        });
    }
}

export function restoreNotebook(notebookId) {
    return (dispatch, getState) => {
        return new Promise((resolve) => {
            let data = { notebookId: notebookId }
            fetch(`${config.url}/trash/restore-notebook`, {
                method: "PUT",
                mode: "cors",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    'x-access-token': getState().userReducer.token
                },
                body: JSON.stringify(data)
            })
                .then(res => res.json())
                .then(json => {
                    if (json.status === "success") {
                        dispatch(_restoreNotebook(notebookId))
                        dispatch(_restoreNotebookNotes(notebookId));
                        resolve(json);
                        return;
                    }
                })
                .catch(e => {
                    console.log(e);
                });
        });
    }
}

export function deleteNotebook(notebookId) {
    return (dispatch, getState) => {
        return new Promise((resolve) => {
            let data = { notebookId: notebookId }
            fetch(`${config.url}/trash/delete-notebook`, {
                method: "PUT",
                mode: "cors",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    'x-access-token': getState().userReducer.token
                },
                body: JSON.stringify(data)
            })
                .then(res => res.json())
                .then(json => {
                    if (json.status === "success") {
                        dispatch(_deleteNotebook(notebookId))
                        dispatch(_deleteNotebookNotes(notebookId));
                        resolve(json);
                        return;
                    }
                })
                .catch(e => {
                    console.log(e);
                });
        });
    }
}