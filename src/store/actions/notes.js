import config from '../../config';

export function _setNotes(notes){
    return {
        type: '_SET_NOTES',
        notes: notes
    };
}

export function addNote(note){
    return {
        type: 'ADD_NOTE',
        note: note
    }
}

export function _setTrashNotes(notes){
    return {
        type: "_SET_TRASH_NOTES",
        notes: notes
    }
}

export function _moveToTrash(note){
    return {
        type: '_MOVE_TO_TRASH',
        note: note
    }
}

export function _deleteNote(note){
    return {
        type: '_DELETE_NOTE',
        note: note
    }
}

export function _restoreNote(note){
    return {
        type: "_RESTORE_NOTE",
        note: note
    }
}

export function _deleteSelectedNotes(notes){
    return {
        type: '_DELETE_SELECTED_NOTES',
        notes: notes
    }
}

export function _restoreSelectedNotes(notes, notebookId){
    return {
        type: "_RESTORE_SELECTED_NOTES",
        notes: notes,
        notebookId: notebookId
    }
}

export function _updateNote(note){
    return {
        type: '_UPDATE_NOTE',
        note: note
    }
}

export function _setActiveNotebookList(notes){
    return {
        type: '_SET_ACTIVE_NOTEBOOK_LIST',
        activeNotebookNotes: notes
    }
}

export function _updateActiveNotebookList(notes){
    return {
        type: '_UPDATE_ACTIVE_NOTEBOOK_LIST',
        notes: notes
    }
}

export function setNotes(){
    return (dispatch, getState) => {
        return new Promise((resolve, reject)=>{
            fetch(`${config.url}/notes`, {
                headers: { 'x-access-token': getState().userReducer.token }
            })
            .then(res => res.json())
            .then((json) => {
                dispatch(_setNotes(json.data.notes));
                return resolve(json);
            })
            .catch((e) => {
                console.log(e);
            });
        });
    }
}

//TODO: decide if you want to place all notes based on notebook in state
export function getNotesByNotebook(notebookId) {
    return (dispatch, getState) => {
        return new Promise((resolve) => {
            fetch(`${config.url}/notes/notebook-notes/${notebookId}`, {
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    'x-access-token': getState().userReducer.token
                }
            })
            .then(res => res.json())
            .then((json) => {
                dispatch(_setActiveNotebookList(json.data.notes));
                if (json && json.data) {
                    resolve(json.data.notes);
                }
            })
            .catch((e) => {
                console.log(e);
            });
        });
    }
}

export function fetchTrashNotes() {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            fetch(`${config.url}/trash`, {
                headers: { 'x-access-token': getState().userReducer.token }
            })
            .then(res => res.json())
            .then((json) => {
                dispatch(_setTrashNotes(json.data.notes));
                return resolve(json);
            })
            .catch((e) => {
                console.log(e);
            });
        });
    }
}


export function moveToTrash(note) {
    return (dispatch, getState) => {
        return new Promise((resolve) => {
            fetch(`${config.url}/notes/trash-note/${note._id}`, {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': getState().userReducer.token
                },
                method: 'PUT',
                mode: 'cors', 
            })
            .then( res => res.json())
            .then((json) => {
                dispatch(_moveToTrash(note));
                return resolve(json);
            })
            .catch( e => console.log(e));
        });
    }
}

export function moveManyToTrash(notes) {
    let data = {
        notes: [],
    }

    notes.forEach(noteId => { data.notes.push(noteId); });
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            fetch(`${config.url}/notes/trash-many-notes`, {
                method: "PUT",
                headers: {
                    'Content-Type': "application/json; charset=utf-8",
                    'x-access-token': getState().userReducer.token
                },
                body: JSON.stringify(data)
            })
                .then(res => res.json())
                .then(json => {
                    if (json.status === "success") {
                        dispatch(_updateActiveNotebookList(notes));
                        return resolve(json);
                    }
                })
                .catch(e => {
                    console.log(e);
                    return reject(e);
                });
        })
    }
}

export function createNote(data) {
    return (dispatch, getState) => {
        return new Promise((resolve) => {
            fetch(`${config.url}/notes`, {
                method: "POST",
                mode: "cors",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    'x-access-token': getState().userReducer.token
                },
                body: JSON.stringify(data)
            })
            .then(res => res.json())
            .then((json) => {
                dispatch(addNote(json.data.note));
                resolve(json);
            })
            .catch((e) => {
                console.log(e);
            });
        });
    }
}


export function updateNote(data) {
    return (dispatch, getState) => {
        return new Promise((resolve) => {
            fetch(`${config.url}/notes/update-note/${data.note._id}`, {
                body: JSON.stringify(data),
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': getState().userReducer.token
                },
                method: 'PUT',
                mode: 'cors',
            })
                .then(res => res.json())
                .then((json) => {
                    dispatch(_updateNote(data.note));
                    resolve(json);
                })
                .catch(e => console.log(e));
        });
    }
}


export function deleteNote(note){
    return (dispatch, getState) => {
        return new Promise((resolve, reject)=>{
            fetch(`${config.url}/notes/${note._id}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': "application/json; charset=utf-8",
                    'x-access-token': getState().userReducer.token
                },
            })
            .then(res => res.json())
            .then(json => {
                dispatch(_deleteNote(note));
                return resolve(json);
            })
            .catch(e => {
                console.log(e);
            });
        })
    }
}


export function restoreNote(note){
    const data = {
        note: { ...note, in_trash: false }
    };
    return(dispatch, getState) => {
        return new Promise((resolve, reject) => {
            fetch(`${config.url}/notes/update-note/${note._id}`,{
                method: "PUT",
                body: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json",
                    "x-access-token": getState().userReducer.token
                }
            })
            .then( res => res.json())
            .then(json => {
                if(json.status === "success"){
                    dispatch(_restoreNote(data.note));
                    return resolve(json);
                }
            })
            .catch(e => {
                console.log(e);
            });
        })
    }
}


export function deleteSelectedNotes(notes) {
    let data = {
        notes: notes
    }
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            fetch(`${config.url}/notes`, {
                method: "DELETE",
                headers: {
                    'Content-Type': "application/json; charset=utf-8",
                    'x-access-token': getState().userReducer.token
                },
                body: JSON.stringify(data)
            })
                .then(res => res.json())
                .then(json => {
                    if(json.status === "success"){
                        dispatch(_deleteSelectedNotes(notes));
                        return resolve(json);
                    }
                })
                .catch(e => {
                    console.log(e);
                });
        })
    }
}

export function restoreSelectedNotes(noteArray, notebookId) {
    let data = {
        notes: [],
        notebookId: notebookId
    } 
    
    noteArray.forEach(noteId => { data.notes.push(noteId);});
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            fetch(`${config.url}/notes/restore-notes`, {
                method: "PUT",
                headers: {
                    'Content-Type': "application/json; charset=utf-8",
                    'x-access-token': getState().userReducer.token
                },
                body: JSON.stringify(data)
            })
                .then(res => res.json())
                .then(json => {
                    if (json.status === "success") {
                        dispatch(_restoreSelectedNotes(noteArray, notebookId));
                        return resolve(json);
                    }
                })
                .catch(e => {
                    console.log(e);
                    return reject(e);
                });
        })
    }
}

export function changeSelectedNotesNotebook(noteArray, notebookId) {
    let data = {
        notes: [],
        notebookId: notebookId
    }
    noteArray.forEach(noteId => { data.notes.push(noteId); });
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            fetch(`${config.url}/notes/change-notebook`, {
                method: "PUT",
                headers: {
                    'Content-Type': "application/json; charset=utf-8",
                    'x-access-token': getState().userReducer.token
                },
                body: JSON.stringify(data)
            })
                .then(res => res.json())
                .then(json => {
                    if (json.status === "success") {
                        dispatch(_updateActiveNotebookList(noteArray));
                        return resolve(json);
                    }
                })
                .catch(e => {
                    console.log(e);
                    return reject(e);
                });
        })
    }
}

export function removeOutDatedTrash (token) {
    fetch(`${config.url}/trash/remove-outdated`, {
        method: "DELETE",
        headers: {
            'Content-Type': "application/json; charset=utf-8",
            'x-access-token': token
        },
    })
        .then(res => res.json())
        .then(json => {
            return
        })
        .catch(e => {
            console.log(e);
        });
}
