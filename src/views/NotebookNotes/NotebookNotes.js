import React, { Component} from 'react';
import './NotebookNotes.scss';
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { moveToTrash, 
        getNotesByNotebook, 
        changeSelectedNotesNotebook,
        moveManyToTrash
         } from '../../store/actions/notes';
import { setActiveNotebook } from '../../store/actions/user'
import ViewHeader from '@components/ViewHeader';
import ScrollContainer from '../../components/scrollContainer/scrollContainer.js';
import MultiSelectHeader from './multiSelectHeader/multiSelectHeader';
import MoveToNoteBookModal from './moveNotebookModal/moveToNotebookModal.js';
import TrashNotesModal from './TrashNotesModal.js';
import ButtonMain from '@components/ButtonMain';
import NoNotesMsg from './NoNotesMsg';
import MultiSelectPreviewCard from '@components/MultiSelectPreviewCard';
import LoadingBar from '@components/LoadingBar';

class NotebookNotes extends Component {

    constructor(props) {
        super(props);
        this.state = {
            mountHeader: false,
            multiSelected: {
                length: 0,
                notes: []
            },
            notebookId: undefined,
            notes: undefined,
            modalOpen: false,
            modalOpen2: false,
            loading: false
        }

        this.moveToTrash = bindActionCreators(moveToTrash, this.props.dispatch);
        this.getNotesByNotebook = bindActionCreators(getNotesByNotebook, this.props.dispatch);
        this.changeSelectedNotesNotebook = bindActionCreators(changeSelectedNotesNotebook, this.props.dispatch);
        this.moveManyToTrash = bindActionCreators(moveManyToTrash, this.props.dispatch);
        this.setActiveNotebook = bindActionCreators(setActiveNotebook, this.props.dispatch);
        this.notebookId = undefined;
        this.notebookTitle = undefined;
    }

    componentDidMount = () => {
        this.setInitialState();
    }

    componentDidUpdate = () => {
        if (this.notebookId !== this.props.match.params.notebookId) {
            this.setInitialState();
        }
        this.mountHeader();
    }

    componentWillUnmount = () => { clearTimeout(this.moutHeaderTimer); }

    setInitialState = () => {
        this.notebookId = this.props.match.params.notebookId;
        this.props.notebooks.forEach((book) => {
            if (book._id === this.notebookId) {
                return this.notebookTitle = book.title;
            }
            return;
        });
        this.setActiveNotebook(this.notebookId, this.notebookTitle);
        this.setState({ loading: true })
        this.getNotesByNotebook(this.notebookId)
            .then(notes => {
                this.setState({
                    loading: false
                })
            });
        
    }

    mountHeader = () => {
        if (this.state.multiSelected.length && !this.state.mountHeader) {
            this.setState({ mountHeader: true });
        } else if (!this.state.multiSelected.length && this.state.mountHeader) {
            // adding a deley to mountHeader to allow for animating out
            this.moutHeaderTimer = setTimeout(() => {
                this.setState({ mountHeader: false });
            }, 400);
        }
    }

    handleChangeSelectedNotesNotebook = (noteArray, notebookId) => {
        return new Promise((resolve) => {
            this.changeSelectedNotesNotebook(noteArray, notebookId)
                .then(() => {
                    resolve();
                    this.setState({
                        multiSelected: {
                            length: 0,
                            notes: []
                        }
                    });
                });
        });
    }

    handleMoveManyToTrash = () => {
        this.moveManyToTrash(this.state.multiSelected.notes)
        .then(()=>{
            this.deselectAll();
        });
    }

    addToMultiSelectList = note => () => {
        let notes = this.state.multiSelected.notes;
        notes.push(note)
        this.setState((state) => ({
            multiSelected: {
                length: (state.multiSelected.length + 1),
                notes: notes
            }
        }));
    }

    removeFromMultiSelectList = id => () => {
        this.setState((state) => ({
            multiSelected: {
                length: (state.multiSelected.length - 1),
                notes: this.state.multiSelected.notes.filter(note => note._id !== id)
            }
        }));
    }

    deselectAll = () => {
        this.setState({
            multiSelected: {
                length: 0,
                notes: []
            }
        },() => {
                const multiselectbtns = Array.from(document.querySelectorAll(".multi-select-btn.active"));
                if (multiselectbtns){
                    multiselectbtns.forEach(btn => {
                        btn.classList.remove("active");
                    })
                }
        });
    }

    openMoveToNoteBookModal = () => {
        this.setState({
            modalOpen: true
        });
    }

    closeMoveToNoteBookModal = () => {
        this.setState({ 
            modalOpen: false,
         });
    }

    closeTrashNotesModal = () => {
        this.setState({
            modalOpen2: false,
        });
    }

    openTrashNotesModal = () => {
        this.setState({
            modalOpen2: true,
        });
    }
    
    setHeader = () => {
        return (
            <React.Fragment>
                <ViewHeader
                    title={<h2>{this.notebookTitle}</h2>}
                    action={<ButtonMain iconStart bgColor="secondary" component={Link} to='/create-note' className='outline-btn'><i className="fas fa-plus"></i> new note</ButtonMain>}
                />
                {this.state.mountHeader ?
                    <MultiSelectHeader
                        multiSelected={this.state.multiSelected}
                        deselectAll={this.deselectAll}
                        handleDeleteSelectedNotes={this.handleDeleteSelectedNotes}
                        openModal={this.openModal}
                        openMoveToNoteBookModal={this.openMoveToNoteBookModal}
                        openTrashNotesModal={this.openTrashNotesModal}
                    />
                    :
                    null}
            </React.Fragment>);
    }

    setNoteList = () => {
        let notes = this.props.notes;
        if (notes && notes.length) {
            notes = notes.map( note =>
                <MultiSelectPreviewCard
                    key={note._id} 
                    note={note}
                    multiSelectActive={this.state.multiSelected.length ? true : false}
                    onSelect={this.addToMultiSelectList(note)}
                    onDeselect={this.removeFromMultiSelectList(note._id)}
                    drawerActions={<button className="drawer-item" onClick={() => {this.moveToTrash(note) }}>place in trash</button>}
                />
                
            );
            return notes;
        } 
        return null
    }

    render = () => {
        const headerItems = this.setHeader();
        const notes = this.setNoteList();
        const newNotebookMsg = <NoNotesMsg ref={(link) => {this.newNoteLink = link}} />;
        const noteBookList = this.props.activeNotebook ? this.props.notebooks.filter(book => book._id !== this.props.activeNotebook._id) : [];

        if(this.state.loading) {
            return <ScrollContainer className="notebook-notes" header={headerItems} ><LoadingBar /></ScrollContainer>;
        }

        return (
            <ScrollContainer className="notebook-notes" header={headerItems} >
                <div className="note-list">
                    <div className="note-list__row">
                        {notes ? notes : newNotebookMsg}
                    </div>
                </div>
                <MoveToNoteBookModal 
                    notebooks={noteBookList}
                    selectedNotes={this.state.multiSelected.notes}
                    changeSelectedNotesNotebook={this.handleChangeSelectedNotesNotebook}
                    modalOpen={this.state.modalOpen}
                    closeModal={this.closeMoveToNoteBookModal}
                />
                <TrashNotesModal 
                    modalOpen={this.state.modalOpen2}
                    handleMoveManyToTrash={this.handleMoveManyToTrash}
                    closeModal={this.closeTrashNotesModal}
                />
            </ScrollContainer>
        );
    }
}

NotebookNotes.propTypes = {
    notebooks: PropTypes.array,
    activeNotebook: PropTypes.object,
    notes: PropTypes.array,
}

const mapStateToProps = (state) => ({
    notebooks: state.userReducer.user.notebooks,
    activeNotebook: state.userReducer.activeNotebook,
    notes: state.notesReducer.activeNotebookNotes,
});

export default connect(mapStateToProps)(NotebookNotes);