import React, {Component} from 'react';
import { Link } from "react-router-dom";
import './NoNotesMsg.scss';
import Card from '@components/Card';
import CardContent from '@components/CardContent';
import ButtonMain from '@components/ButtonMain';
import NewNoteIcon from '@assets/imgs/new-note.svg';

class NoNotesMsg extends Component {

    componentDidMount(){
        if (this.noteLink){
            this.noteLink.focus();
        }
    }

    render = () => {
        return (
            <Card className="no-notes-msg">
                <CardContent>
                    <img className="no-notes-msg__card-icon" src={NewNoteIcon} alt=""/>
                    <h3>Add your first note</h3>
                    <ButtonMain iconStart component={Link} bgColor="primary" to='/create-note' className='outline-btn' ref={link => this.noteLink = link}><i className="fas fa-plus"></i> new note</ButtonMain>
                </CardContent>
            </Card>
        );
    }
};

export default NoNotesMsg;