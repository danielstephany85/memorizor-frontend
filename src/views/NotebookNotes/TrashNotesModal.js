import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from '../../components/modal/Modal';

class TrashNotesModal extends Component {

    handleAddNotebookToTrash = () => {
        this.props.handleMoveManyToTrash();
        this.props.closeModal();
    }

    render = () => {

        return (
            <Modal modalOpen={this.props.modalOpen} closeModal={this.props.closeModal}>
                <div>
                    <button className="modal-close" onClick={this.props.closeModal}>close</button>
                    <p>Are you sure you would like to the selected notes in the trash</p>
                    <div className="modal-btn-container">
                        <button type="button" className="main-btn" onClick={() => { this.handleAddNotebookToTrash()}}>delete</button>
                    </div>
                </div>
            </Modal>
        );
    }
}

TrashNotesModal.propTypes = {
    titel: PropTypes.string,
    id: PropTypes.string,
    addNotsToTrash: PropTypes.func
}

export default TrashNotesModal;