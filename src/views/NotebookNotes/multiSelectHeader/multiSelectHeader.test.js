import {mount} from 'enzyme';
import React from 'react';
import MultiSelectHeader from './multiSelectHeader';

describe('multiSelectHeader tests', () => {

    test('is delicious', () => {
        const wrapper = mount(<MultiSelectHeader multiSelected={{length: 1, notes: []}}
            deselectAll={()=>{}}
            handleDeleteSelectedNotes={()=>{}}/>);
            setTimeout(()=>{
                expect(wrapper.find(".animate-in").at(0)).toHaveLength(1);
            },60);
    });

});