import React, { Component } from 'react';
import NewNoteCard from '@components/NewNoteCard';

class NewNoteView extends Component {

    render = () => {
        return (
            <NewNoteCard history={this.props.history}/>
        );
    }
}

export default NewNoteView;