import React from 'react';
import './Hero.scss';
import phone from '@assets/imgs/home/phonebody.png';
import computer from '@assets/imgs/home/computer.png';
import ButtonMain from '@components/ButtonMain';
import { Link } from "react-router-dom"
import Type from '@components/Type';
import BgRepeated from '@components/BgRepeated';

const Hero = (props) => {

    return (
        <section className="hero">
            <BgRepeated bgColor="pink" />
            <img className="hero__phone" src={phone} alt="memorizor on a mobile phone"/>
            <img className="hero__computer" src={computer} alt="memorizor on a laptop" />
            <div className="hero__content">
                <Type element="h2">Note taking made easy</Type>
                <Type element="h3">Thank you for taking the time to check out memorizor, but please note that this is a demo project, do not use as your note taking solution</Type>
                <ButtonMain to="/create-account" bgColor="secondary" component={Link} size="lg">sign-up</ButtonMain>
            </div>
        </section>
    );
}

export default Hero;