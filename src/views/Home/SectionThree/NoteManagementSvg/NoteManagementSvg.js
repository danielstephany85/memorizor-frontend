import React from 'react';
import './NoteManagementSvg.scss';

const NoteManagementSvg = (props) => {

    return (
        <svg className="note-management-svg" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 444.65 360.22">
            <g id="note-3" className="st0">
                <path className="st1" d="M430.24,301.99h-81.58c-3.31,0-6-2.69-6-6V185.25c0-3.31,2.69-6,6-6h81.58c3.31,0,6,2.69,6,6v110.74C436.24,299.3,433.56,301.99,430.24,301.99z"/>
                <rect x="350.49" y="187.98" className="st2" width="37.19" height="3.43" />
                <rect x="350.49" y="195.62" className="st2" width="76.97" height="3.43" />
                <rect x="350.49" y="203.27" className="st2" width="76.97" height="3.43" />
                <rect x="350.49" y="210.91" className="st2" width="76.97" height="3.43" />
                <rect x="350.49" y="218.55" className="st2" width="76.97" height="3.43" />
                <rect x="350.49" y="226.2" className="st2" width="55.25" height="3.43" />
                <rect x="350.49" y="233.84" className="st2" width="76.97" height="3.43" />
                <rect x="350.49" y="241.48" className="st2" width="76.97" height="3.43" />
                <rect x="350.49" y="249.13" className="st2" width="45.07" height="3.43" />
            </g>
            <g id="note-2" className="st0">
                <path className="st1" d="M267.86,129.67h-81.58c-3.31,0-6-2.69-6-6V12.93c0-3.31,2.69-6,6-6h81.58c3.31,0,6,2.69,6,6v110.74C273.86,126.99,271.17,129.67,267.86,129.67z"/>
                <rect x="188.11" y="15.67" className="st2" width="37.19" height="3.43" />
                <rect x="188.11" y="23.31" className="st2" width="76.97" height="3.43" />
                <rect x="188.11" y="30.95" className="st2" width="76.97" height="3.43" />
                <rect x="188.11" y="38.6" className="st2" width="76.97" height="3.43" />
                <rect x="188.11" y="46.24" className="st2" width="76.97" height="3.43" />
                <rect x="188.11" y="53.88" className="st2" width="55.25" height="3.43" />
                <rect x="188.11" y="61.52" className="st2" width="76.97" height="3.43" />
                <rect x="188.11" y="69.17" className="st2" width="76.97" height="3.43" />
                <rect x="188.11" y="76.81" className="st2" width="45.07" height="3.43" />
            </g>
            <g id="note-1" className="st0">
                <path className="st1" d="M106.74,299.4H25.17c-3.31,0-6-2.69-6-6V182.66c0-3.31,2.69-6,6-6h81.58c3.31,0,6,2.69,6,6V293.4C112.74,296.72,110.06,299.4,106.74,299.4z"/>
                <rect x="26.99" y="185.4" className="st2" width="37.19" height="3.43" />
                <rect x="26.99" y="193.04" className="st2" width="76.97" height="3.43" />
                <rect x="26.99" y="200.68" className="st2" width="76.97" height="3.43" />
                <rect x="26.99" y="208.33" className="st2" width="76.97" height="3.43" />
                <rect x="26.99" y="215.97" className="st2" width="76.97" height="3.43" />
                <rect x="26.99" y="223.61" className="st2" width="55.25" height="3.43" />
                <rect x="26.99" y="231.26" className="st2" width="76.97" height="3.43" />
                <rect x="26.99" y="238.9" className="st2" width="76.97" height="3.43" />
                <rect x="26.99" y="246.54" className="st2" width="45.07" height="3.43" />
            </g>
            <g id="trash">
                <g>
                    <path className="st3" d="M436.99,195.14l-10.61,102.51c0,2.92-2.37,5.3-5.3,5.3h-63.09c-2.93,0-5.3-2.38-5.3-5.3l-10.61-102.51c0-2.93,1.82-5.47,4.75-5.47h85.41C435.17,189.67,436.99,192.21,436.99,195.14z"/>
                    <path className="st4" d="M421.08,304.45h-63.09c-3.72,0-6.75-3.01-6.8-6.72l-10.61-102.59c0-3.97,2.69-6.97,6.25-6.97h85.41c3.56,0,6.25,3,6.25,6.97l-0.01,0.15l-10.6,102.44C427.84,301.45,424.8,304.45,421.08,304.45z M343.58,195.07l10.61,102.58c0,2.09,1.7,3.8,3.8,3.8h63.09c2.1,0,3.8-1.71,3.8-3.8l0.01-0.15l10.6-102.43c-0.03-1.89-1.04-3.9-3.25-3.9h-85.41C344.72,191.17,343.61,193.12,343.58,195.07z"/>
                </g>
                <g>
                    <path className="st3" d="M441.54,176.19v4.76c0,1.46-1.18,2.65-2.65,2.65h-98.7c-1.46,0-2.65-1.19-2.65-2.65v-4.76c0-1.47,1.19-2.65,2.65-2.65h34.23v-2.73c0-1.46,1.18-2.65,2.65-2.65h24.95c1.46,0,2.65,1.19,2.65,2.65v2.73h34.22C440.36,173.54,441.54,174.72,441.54,176.19z"/>
                    <path className="st4" d="M438.89,185.09h-98.7c-2.29,0-4.15-1.86-4.15-4.15v-4.76c0-2.29,1.86-4.15,4.15-4.15h32.73v-1.23c0-2.29,1.86-4.15,4.15-4.15h24.95c2.29,0,4.15,1.86,4.15,4.15v1.23h32.72c2.29,0,4.15,1.86,4.15,4.15v4.76C443.04,183.23,441.18,185.09,438.89,185.09z M340.19,175.04c-0.63,0-1.15,0.52-1.15,1.15v4.76c0,0.63,0.52,1.15,1.15,1.15h98.7c0.63,0,1.15-0.52,1.15-1.15v-4.76c0-0.63-0.52-1.15-1.15-1.15h-35.72v-4.23c0-0.63-0.52-1.15-1.15-1.15h-24.95c-0.63,0-1.15,0.52-1.15,1.15v4.23H340.19z"/>
                </g>
                <path className="st5" d="M394.54,211.71v89c0,0.2-0.01,0.4-0.04,0.6h-9.92c-0.03-0.2-0.04-0.4-0.04-0.6v-89c0-2.76,2.24-5,5-5c1.38,0,2.63,0.56,3.53,1.46C393.98,209.08,394.54,210.33,394.54,211.71z"/>
                <path className="st5" d="M373.29,300.71c0.01,0.2,0.01,0.4,0,0.6h-9.93c-0.04-0.2-0.06-0.4-0.07-0.6l-4.67-89c-0.14-2.76,1.98-5,4.74-5c1.38,0,2.66,0.56,3.61,1.46c0.96,0.91,1.58,2.16,1.65,3.54L373.29,300.71z"/>
                <path className="st5" d="M420.63,211.43c0,0.1,0,0.19-0.01,0.28l-4.66,89c-0.01,0.2-0.03,0.4-0.07,0.6h-9.93c-0.01-0.2-0.01-0.4,0-0.6l4.66-89c0.15-2.76,2.51-5,5.27-5C418.56,206.71,420.63,208.8,420.63,211.43z"/>
            </g>
            <g id="left-notebook">
                <g>
                    <path className="st6" d="M115.5,177.78v120.56c0,2.82-2.29,5.1-5.1,5.1H22.41c-0.06-0.03-0.12-0.05-0.19-0.08c-1.18-0.47-2.91-0.77-4.82-0.77c-1.98,0-3.74,0.31-4.92,0.81c-0.04,0.01-0.07,0.03-0.1,0.04H9.3c-2.82,0-5.11-2.28-5.11-5.1V177.78c0-2.82,2.29-5.11,5.11-5.11h3.18c1.18,0.48,2.95,0.8,4.92,0.8c1.91,0,3.65-0.3,4.82-0.77c0.03-0.01,0.06-0.02,0.09-0.03h88.09C113.21,172.67,115.5,174.96,115.5,177.78z"/>
                    <path className="st4" d="M110.4,304.94H22.06l-0.43-0.2c-1-0.4-2.6-0.65-4.22-0.65c-1.72,0-3.3,0.25-4.33,0.69l-0.12,0.05c-0.03,0.01-0.07,0.03-0.1,0.04l-0.22,0.07H9.3c-3.65,0-6.61-2.96-6.61-6.6V177.78c0-3.64,2.97-6.61,6.61-6.61h3.47l0.27,0.11c2.1,0.85,6.54,0.86,8.62,0.03l0.4-0.14h88.34c3.64,0,6.6,2.97,6.6,6.61v120.56C117,301.98,114.04,304.94,110.4,304.94z M22.71,301.94h87.69c1.98,0,3.6-1.62,3.6-3.6V177.78c0-1.99-1.62-3.61-3.6-3.61H22.58c-1.3,0.5-3.2,0.8-5.17,0.8c-1.93,0-3.8-0.29-5.2-0.8H9.3c-1.99,0-3.61,1.62-3.61,3.61v120.56c0,1.98,1.62,3.6,3.61,3.6h2.79c1.4-0.55,3.27-0.85,5.31-0.85C19.42,301.09,21.3,301.39,22.71,301.94z"/>
                </g>
                <path className="st7" d="M22.22,174.29v127.47c-1.18-0.46-2.91-0.75-4.82-0.75c-1.98,0-3.74,0.3-4.92,0.79V174.26c1.18,0.47,2.95,0.78,4.92,0.78C19.31,175.04,21.05,174.75,22.22,174.29z"/>
                <path className="st7" d="M100.56,195.16H35.79c-1.66,0-3-1.34-3-3v-7.72c0-1.66,1.34-3,3-3h64.77c1.66,0,3,1.34,3,3v7.72C103.56,193.81,102.22,195.16,100.56,195.16z"/>
            </g>
            <g id="top-notebok">
                <g>
                    <path className="st8" d="M277.48,8.57v119.47c0,2.8-2.27,5.06-5.06,5.06h-87.18c-0.02-0.01-0.04-0.02-0.07-0.03c-1.18-0.5-2.93-0.82-4.9-0.82c-1.95,0-3.7,0.31-4.87,0.81c-0.04,0.01-0.08,0.02-0.11,0.04h-3.05c-2.79,0-5.06-2.26-5.06-5.06V8.57c0-2.79,2.27-5.06,5.06-5.06h3.16c1.17,0.49,2.92,0.8,4.87,0.8c1.95,0,3.69-0.31,4.87-0.8h87.28C275.21,3.51,277.48,5.78,277.48,8.57z"/>
                    <path className="st4" d="M272.42,134.6h-87.53l-0.31-0.15c-1.05-0.45-2.62-0.7-4.31-0.7c-1.67,0-3.27,0.26-4.28,0.69l-0.01,0l-0.23,0.16h-3.51c-3.62,0-6.56-2.94-6.56-6.56V8.57c0-3.62,2.94-6.56,6.56-6.56h3.46l0.28,0.12c1.02,0.43,2.63,0.68,4.29,0.68c1.68,0,3.25-0.25,4.3-0.69l0.28-0.11h87.58c3.62,0,6.56,2.94,6.56,6.56v119.47C278.98,131.65,276.03,134.6,272.42,134.6z M185.53,131.6h86.88c1.96,0,3.56-1.6,3.56-3.56V8.57c0-1.96-1.6-3.56-3.56-3.56h-87c-1.39,0.52-3.2,0.8-5.15,0.8c-1.96,0-3.78-0.28-5.16-0.8h-2.87c-1.96,0-3.56,1.6-3.56,3.56v119.47c0,1.96,1.6,3.56,3.56,3.56H175c1.39-0.55,3.24-0.85,5.26-0.85C182.27,130.75,184.13,131.05,185.53,131.6z"/>
                </g>
                <path className="st9" d="M185.17,5.09v126.35c-1.18-0.49-2.93-0.8-4.9-0.8c-1.95,0-3.7,0.3-4.87,0.79V5.09c1.17,0.48,2.92,0.78,4.87,0.78c1.95,0,3.69-0.3,4.87-0.78H185.17z"/>
                <path className="st9" d="M263.42,26.39h-64.77c-1.66,0-3-1.34-3-3v-7.72c0-1.66,1.34-3,3-3h64.77c1.66,0,3,1.34,3,3v7.72C266.42,25.05,265.08,26.39,263.42,26.39z"/>
            </g>
        </svg>

    );
}

export default NoteManagementSvg;