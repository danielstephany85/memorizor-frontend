import React, { useState} from 'react';
import './SectionThree.scss';
import SkewBlock from '@components/SkewBlock';
import NoteManagementSvg from './NoteManagementSvg';
import SplitSection from '@components/SplitSection';
import { Waypoint } from 'react-waypoint';
import SetClasses from '@components/_utils/SetClasses';
import ArticleSection from '@components/ArticleSection';
import Type from '@components/Type';

const SectionThree = () => {
    const [inView, setInView] = useState(false);
    const svgContainterBaseClass = "svg-container";
    const svgContainterClasses = SetClasses(svgContainterBaseClass, inView ? "svgActive" : "");

    const skewBlock = (
        <SkewBlock bgColor="pink">
            <Waypoint bottomOffset="300px" onEnter={() => { setInView(true) }} onLeave={() => { setInView(false) }}>
                <div className={svgContainterClasses}>
                    <NoteManagementSvg />
                </div>
            </Waypoint>
        </SkewBlock>
    );

    const rightSection = (
        <ArticleSection right>
            <Type element="h3">Note management</Type>
            <Type element="p" like="lg-copy">Easily move notes between notebooks, place notes in the trash, and restore notes from the trash into any desired notebook.</Type>
        </ArticleSection>
    );

    return (
        <SplitSection className="section-three"
            leftSection={skewBlock}
            rightSection={rightSection}
        />
    );
}

export default SectionThree;