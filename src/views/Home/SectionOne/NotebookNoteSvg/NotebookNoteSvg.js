import React from 'react';	
import './NotebookNoteSvg.scss';
import SetClasses from '@components/_utils/SetClasses';

const NotebookNoteSvg = (props) => {
	const { className } = props;
	const BASE_CLASS = 'NotebookNoteSvg-svg';
	const classes = SetClasses(BASE_CLASS, className);

	return ( 
		<svg version="1.1" className={classes} x="0px" y="0px" viewBox="0 0 512 512" >
			<g id="note1">
				<path className="st0" d="M413.34,499.54H87.99c-11.72,0-21.24-9.84-21.27-21.98L65.65,35.72c-0.03-12.14,9.45-21.98,21.17-21.98h325.35c11.72,0,21.24,9.84,21.27,21.98l1.07,441.84C434.54,489.7,425.06,499.54,413.34,499.54z"/>
				<rect x="97.56" y="74.87" className="st1" width="305.47" height="12.82" />
				<rect x="97.56" y="50.6" className="st1" width="134.7" height="12.82" />
				<rect x="97.56" y="99.8" className="st1" width="305.47" height="12.82" />
				<rect x="97.56" y="126.61" className="st1" width="305.47" height="12.82" />
				<rect x="97.56" y="153.43" className="st1" width="305.47" height="12.82" />
				<rect x="97.56" y="180.24" className="st1" width="201.95" height="12.82" />
				<rect x="97.56" y="207.05" className="st1" width="305.47" height="12.82" />
				<rect x="97.56" y="233.87" className="st1" width="305.47" height="12.82" />
				<rect x="97.56" y="260.68" className="st1" width="305.47" height="12.82" />
				<rect x="97.56" y="287.5" className="st1" width="305.47" height="12.82" />
				<rect x="97.56" y="314.31" className="st1" width="220.8" height="12.82" />
			</g>
		</svg>
	);
}

export default NotebookNoteSvg;
