import React, {Component} from 'react';
import './SectionOne.scss';
import SkewBlock from '@components/SkewBlock';
import Notebooksvg from './Notebooksvg';
import NotebookNoteSvg from './NotebookNoteSvg';
import SplitSection from '@components/SplitSection';
import { Waypoint } from 'react-waypoint';
import SetClasses from '@components/_utils/SetClasses';
import ArticleSection from '@components/ArticleSection';
import Type from '@components/Type';

class SectionOne extends Component {
    constructor(props){
        super(props);
        this.state = { 
            inView: false,
            svgCanAnimate: false
         }
    }
    
    setInView = (inView) => (data) => {
        if (data && !data.previousPosition){
            this.setState({
                inView: false,
            });
        } else {
            let canAnimate = (inView || this.state.svgCanAnimate) ? true : false;

            this.setState({
                inView: inView,
                svgCanAnimate: canAnimate
            });
        }

    }

    render(){
        const svgContainterBaseClass = "notebook-svg-container";
        let svgContainterClasses = SetClasses(svgContainterBaseClass, this.state.inView ? "svgActive" : "");
        svgContainterClasses = this.state.svgCanAnimate ? svgContainterClasses + " can-animate" : svgContainterClasses;

        const skewBlock = (
            <SkewBlock bgColor="yellow">
                <Waypoint bottomOffset="300px" onEnter={this.setInView(true)} onLeave={this.setInView(false)}>
                    <div className="svg-wrapper">
                        <div className={svgContainterClasses}>
                            <Notebooksvg />
                            <NotebookNoteSvg className="notebook-note-1" />
                            <NotebookNoteSvg className="notebook-note-2" />
                            <NotebookNoteSvg className="notebook-note-3" />
                        </div>
                    </div>
                </Waypoint>
            </SkewBlock>
        );

        const rightSection = (
            <ArticleSection right>
                <Type element="h3">Easily stay organized </Type>
                <Type element="p" like="lg-copy">Memorizor gives you the ability to organize your notes into seperate notebooks. </Type>
            </ArticleSection>
        );

        return (
            <SplitSection className="section-one"
                leftSection={skewBlock}
                rightSection={rightSection}
            / >
        );
    }
}

export default SectionOne;