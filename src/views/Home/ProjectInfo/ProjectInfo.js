import React from 'react';
import './ProjectInfo.scss';
import Type from '@components/Type';
import BgRepeated from '@components/BgRepeated';

const ProjectInfo = (props) => {

    return (
        <section className="text-card-section">
            <BgRepeated bgColor="blue" />
            <div className="text-card-section__content">
                <Type element="h2" like="h3">More about this project </Type>
                <Type element="p">Thank you for taking the time to checkout Memorizor.  Memorizor is a simple note taking application that allows users to do the following:</Type>
                <ul>
                    <li>Create note using  a rich text editor</li>
                    <li>Organize notes by creating notebooks</li>
                    <li>Easily move one note or many notes at once to a different notebook</li>
                    <li>Trash management, when a note is deleted it is placed into the trash for up to 30 day so that it may be restored if needed.</li>
                </ul>
                <Type element="p">Memorizor’s Backend is a rest api created with the following technologies: Node.js, Express, Mongodb.</Type>
                <Type element="p">Memorizor’s Frontend was created with the following technologies:  React, Redux, Webpack, Quill.js, SCSS/CSS, HTML.</Type>
            </div>
        </section>
    );
}

export default ProjectInfo;