import React, {Component} from 'react';
import Hero from './Hero';
import SectionOne from './SectionOne';
import SectionTwo from './SectionTwo';
import SectionThree from './SectionThree';
import ProjectInfo from './ProjectInfo';

class Home extends Component {

    render = () => {
        return (
            <React.Fragment>
                <div>
                    <Hero />
                    <SectionOne /> 
                    <SectionTwo />
                    <SectionThree />
                    <ProjectInfo />
                </div>
            </React.Fragment>
        );
    }
}

export default Home;