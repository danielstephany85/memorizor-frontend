import React from 'react';
import "./RichTextNote.scss"

const RichTextNote = (props) => {

    return (
        <svg version="1.1" className="rich-text-note-svg" id="Layer_1"  x="0px" y="0px" viewBox="0 0 512 512" >
            <g>
                <path className="st0" d="M446.82,499.1H66.13c-6.63,0-12-5.37-12-12V16.69c0-6.63,5.37-12,12-12h380.7c6.63,0,12,5.37,12,12V487.1C458.82,493.73,453.45,499.1,446.82,499.1z"/>
                <path id="blockquote" className="st1" d="M414.75,368.51H91.38c-3.31,0-6-2.69-6-6v-52.44c0-3.31,2.69-6,6-6h323.38c3.31,0,6,2.69,6,6v52.44C420.75,365.83,418.07,368.51,414.75,368.51z"/>
                <g id="ul-4">
                    <circle className="st2" cx="91.25" cy="265.14" r="5.87" />
                    <rect x="111.85" y="259.28" className="st2" width="258.07" height="11.74" />
                </g>
                <rect id="ul-3" x="111.85" y="232.52" className="st2" width="202.98" height="11.74" />
                <g id="ul-2">
                    <circle className="st2" cx="91.25" cy="212.02" r="5.87" />
                    <rect x="111.85" y="206.15" className="st2" width="308.91" height="11.74" />
                </g>
                <g id="ul-1">
                    <rect x="111.85" y="176.52" className="st2" width="202.98" height="11.74" />
                    <circle className="st2" cx="91.25" cy="182.39" r="5.87" />
                </g>
                <rect id="block-4" x="85.38" y="146.89" className="st2" width="202.98" height="11.74" />
                <rect id="block-3" x="85.38" y="116.61" className="st2" width="335.38" height="11.74" />
                <rect id="block-2" x="85.38" y="86.32" className="st2" width="335.38" height="11.74" />
                <rect id="block-1" x="85.38" y="48.34" className="st2" width="162.37" height="19.43" />
            </g>
        </svg>
    );
}

export default RichTextNote;