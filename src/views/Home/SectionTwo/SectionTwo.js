import React, {useState} from 'react';
import './SectionTwo.scss';
import SkewBlock from '@components/SkewBlock';
import RichTextNote from './RichTextNote';
import SplitSection from '@components/SplitSection';
import { Waypoint } from 'react-waypoint';
import SetClasses from '@components/_utils/SetClasses';
import ArticleSection from '@components/ArticleSection';
import Type from '@components/Type';

const SectionTwo = () => {
    const [inView, setInView] = useState(false);
    const svgContainterBaseClass = "svg-container";
    const svgContainterClasses = SetClasses(svgContainterBaseClass, inView ? "svgActive" : "");

    const skewBlock = (
        <SkewBlock flip bgColor="blue">
            <Waypoint bottomOffset="300px" onEnter={() => { setInView(true) }} onLeave={() => { setInView(false) }}>
                <div className={svgContainterClasses}>
                    <RichTextNote />
                </div>
            </Waypoint>
        </SkewBlock>
    );

    const leftSection = (
        <ArticleSection left>
            <Type element="h3">Beautifull rich text</Type>
            <Type element="p" like="lg-copy">Dont feel constrained by plain text, Memorior offers a full featured richtext soultion giving the ability to create more expressive notes.</Type>
        </ArticleSection>
    );

    return (
        <SplitSection className="section-two"
            leftSection={leftSection}
            rightSection={skewBlock}
        />
    );
}

export default SectionTwo;