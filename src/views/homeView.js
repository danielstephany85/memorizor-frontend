import React, {Component} from 'react';

class Home extends Component {

    render = () => {
        return(
            <section className="content-section">
                <div className="inner-content">
                    <h1>Home page</h1>
                </div>
            </section>
        );
    }
}

export default Home;