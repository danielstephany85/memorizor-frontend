import React, {Component} from 'react';
import ScrollContainer from '../../components/scrollContainer/scrollContainer.js';
import ViewHeader from '@components/ViewHeader';
import Input from '../../components/input/Input.js';
import Label from '../../components/label/Label.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updatePassword } from '../../store/actions/user';

class PasswordChange extends Component {
    constructor(props){
        super(props);

        this.state = {
            newPass: '',
            newPass2: '',
            currentPass: '',
            updated: false
        }
        this.updatePassword = bindActionCreators(updatePassword, this.props.dispatch);
    }

    hasValidFields = () => {
        const {newPass, newPass2, currentPass} = this.state
        let errors = {};
        let hasErrors = false;
        if (!newPass){
            errors.newPassErr = "field is Required";
            hasErrors = true;
        }
        if (!newPass2) {
            errors.newPass2Err = "field is Required";
            hasErrors = true;
        }
        if (!currentPass) {
            errors.currentPassErr = "field is Required";
            hasErrors = true;
        }
        if (hasErrors){
            this.setState({...errors});
            return false;
        }
        if (newPass !== newPass2) {
            this.setState({newPass2Err: "the new password fields do not match"});
            return false;
        }
        return true;
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const {currentPass, newPass, newPass2} = this.state

        this.setState({
            newPassErr: "",
            newPass2Err: "",
            currentPassErr: ""
        });
        if (this.hasValidFields()){
            let data = {
                currentPass: currentPass,
                newPass: newPass,
                newPass2: newPass2
            }
            this.updatePassword(data)
            .then(res => {
                console.log(res);
                if (res.status === 'success'){
                    console.log('updated');
                    this.setState({ updated: true});
                }
                if (res.status === 'error' && res.data.message === 'incorrect current password'){
                    this.setState({ currentPassErr: "does not match existing password"});
                }
            });
        }
    }

    updateValue = name => (e) => {
        this.setState({[name]: e.target.value});
    }

    render = () => {
        return (
            <ScrollContainer header={<ViewHeader title={<h2>Change Password</h2>} />} >
                <section className="content-section">
                    <div className="inner-content inner-content--no-mt">
                        <div className="flat-content">
                            <form onSubmit={this.handleSubmit}>
                                <div className="form-content">
                                    <div className={`form-item ${this.state.currentPassErr ? 'error' : ''}`}>
                                        <Label htmlFor="currentPass">Current Password</Label>
                                        <Input borderBottom type="password" name="currentPass" value={this.state.currentPass} onChange={this.updateValue("currentPass")} />
                                        {this.state.currentPassErr ? <span className="error-msg">{this.state.currentPassErr}</span> : ''}
                                    </div>
                                    <div className={`form-item ${this.state.newPassErr ? 'error' : ''}`}>
                                        <Label htmlFor="email">New Password</Label>
                                        <Input borderBottom type="password" name="newPass" value={this.state.newPass} onChange={this.updateValue("newPass")} />
                                        {this.state.newPassErr ? <span className="error-msg">{this.state.newPassErr}</span> : ''}
                                    </div>
                                    <div className={`form-item ${this.state.newPass2Err ? 'error' : ''}`}>
                                        <Label htmlFor="email">Re-enter New Password</Label>
                                        <Input borderBottom type="password" name="newPass2" value={this.state.newPass2} onChange={this.updateValue("newPass2")} />
                                        {this.state.newPass2Err ? <span className="error-msg">{this.state.newPass2Err}</span> : ''}
                                    </div>
                                    <div className='form-item'>
                                        <button className='form-btn' type="submit">update</button>
                                        {this.state.updated ? <i class="fas fa-check"></i> : null}
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </ScrollContainer>
        );
    }

}

export default connect()(PasswordChange);