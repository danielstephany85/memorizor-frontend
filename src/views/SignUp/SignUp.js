import React, {Component} from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { creatUser } from '@store/actions/user';
import './SignUp.scss';
import BgRepeated from '@components/BgRepeated';
import ButtonMain from '@components/ButtonMain';

class SignUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            email: '',
            password1: '',
            password2: '',
            errors: undefined,
            loading: false
        }
        this.creatUser = bindActionCreators(creatUser, this.props.dispatch);
    }


    hasValidInputs = () => {
        let errData = {};
        if(!this.state.userName){
            errData.userNameErr = "User name is required";
        }
        if(!this.state.email){
            errData.emailErr = "Email is required"
        }
        if (!this.state.email.includes("@") || !this.state.email.includes(".")){
            errData.emailErr = "not a valid email"
        }
        if(!this.state.password1){
            errData.password1Err = 'password is required';
        } else if (this.state.password1.length < 8 || this.state.password1.length > 20 ){
            errData.password1Err = 'password must be between 8 and 20 characters';
        }
        if (!this.state.password2) {
            errData.password2Err = 're-enter password is required';
            
        } else if (this.state.password1 !== this.state.password2) {
            errData.password2Err = 'passwords do not match';
        }
        if (Object.keys(errData).length){
            this.setState({...errData});
            return false;
        }
        return true;
    }

    handleFetch = () => {
        const data = {
            name: this.state.userName,
            email: this.state.email,
            password: this.state.password1
        }
        this.setState({ loading: true });
        this.creatUser(data)
        .then(json => {
            localStorage.setItem('token', json.data.token);
            this.props.history.push('/notebooks');
        })
        .catch(json => {
            this.setState({
                emailErr: json.data.message,
                loading: false
            });
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            userNameErr: '',
            emailErr: '',
            password1Err: '',
            password2Err: '',
        });

        if (this.hasValidInputs()){
            this.handleFetch();
        }
    }

    render = () => {
        return(
            <section className="sign-up-page content-section">
                <BgRepeated bgColor="blue" />
                <div className="inner-content">
                    <div className="form-card">
                        <h2>Create an Account</h2>
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-content">
                                <div className={`form-item ${this.state.userNameErr? 'error':''}`}>
                                    <label htmlFor="name">User Name</label>
                                    <input type="text" id="name" name="name" value={this.state.userName} onChange={(e) => { this.setState({ userName: e.target.value }) }} autoFocus/>
                                    {this.state.userNameErr ? <span className="error-msg">{this.state.userNameErr}</span> : ''}
                                </div>
                                <div className={`form-item ${this.state.emailErr ? 'error' : ''}`}>
                                    <label htmlFor="email">Email</label>
                                    <input type="text" id="email" name="email" value={this.state.email} onChange={(e) => { this.setState({ email: e.target.value }) }}/>
                                    {this.state.emailErr ? <span className="error-msg">{this.state.emailErr}</span> : ''}
                                </div>
                                <div className={`form-item ${this.state.password1Err ? 'error' : ''}`}>
                                    <label htmlFor="password1">Password</label>
                                    <input type="password" id="password1" name="password1" value={this.state.password1} onChange={(e) => { this.setState({ password1: e.target.value }) }}/>
                                    {this.state.password1Err ? <span className="error-msg">{this.state.password1Err}</span> : ''}
                                </div>
                                <div className={`form-item ${this.state.password2Err ? 'error' : ''}`}>
                                    <label htmlFor="password2">Re-enter Password</label>
                                    <input type="password" id="password2" name="password2" value={this.state.password2} onChange={(e) => { this.setState({ password2: e.target.value }) }}/>
                                    {this.state.password2Err ? <span className="error-msg">{this.state.password2Err}</span> : ''}
                                </div>
                                <ButtonMain type="submit" bgColor="secondary" size="lg" loading={this.state.loading}>login</ButtonMain>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        );
    }
}

export default connect()(SignUp);