import React, {Component} from 'react';
import Note from '@components/Note';
import {connect} from 'react-redux';
import config from '../config';

class NoteView extends Component {

    constructor(props){
        super(props);
        this.state = {
            note: undefined
        }
    }

    componentDidMount = () => {
        this.setState({ noteId: this.props.match.params.noteId },()=>{
            this.fetchNote();
        });
    }

    componentWillUpdate = () => {
        if (this.state.noteId !== this.props.match.params.noteId){
            this.setState({ noteId: this.props.match.params.noteId });
        }
    }

    fetchNote = () => {
        fetch(`${config.url}/notes/${this.state.noteId}`, {
            headers: { 'x-access-token': this.props.token}
        })
        .then(res => res.json() )
        .then( json =>{
            this.setState({note: json.data.note});
        })
        .catch(e => {
            this.props.history.push('/notebooks');
            console.log(e);
        });
    }
    
    render = () => {
        return(
            <div className="view-container">
                {this.state.note ? <Note note={this.state.note} history={this.props.history} /> : 'loading...'}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    token: state.userReducer.token,
})

export default connect(mapStateToProps)(NoteView);