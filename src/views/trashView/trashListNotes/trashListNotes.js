import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MultiSelectPreviewCard from '@components/MultiSelectPreviewCard';

class TrashListNotes extends Component {

    handleAddToMultiSelectList = note => () => {
        this.props.addToMultiSelectList(note);
    }

    handleRemoveFromMultiSelectList = note => () => {
        this.props.removeFromMultiSelectList(note._id);
    }

    render = () => {
        let notes = this.props.notes;

        if (this.props.match.params.notebookId) {
            notes = notes.filter(note => (note.notebookId === this.props.match.params.notebookId));
        }

        if (notes) {
            notes = notes.map( note => 
                <MultiSelectPreviewCard
                    key={note._id}
                    note={note}
                    multiSelectActive={this.props.multiSelectActive}
                    onSelect={this.handleAddToMultiSelectList(note)}
                    onDeselect={this.handleRemoveFromMultiSelectList(note._id)}
                    drawerActions={[
                        <button key={1} className="drawer-item" onClick={() => { this.props.deleteNote(note) }}>delete note</button>,
                        <button key={2} className="drawer-item" onClick={() => { this.props.openModal(note) }}>restore note</button>
                    ]}
                />
            );
        } else {
            notes = <span>Trash is empty</span>
        }

        return (
            <div className="note-list">
                <div className="note-list__row">
                    {notes}
                </div>
            </div>
        );
    }
}

TrashListNotes.propTypes = {
    notes: PropTypes.array,
    addToMultiSelectList: PropTypes.func,
    removeFromMultiSelectList: PropTypes.func,
    multiSelectActive: PropTypes.bool,
    deleteNote: PropTypes.func,
    openModal: PropTypes.func
}

export default TrashListNotes;