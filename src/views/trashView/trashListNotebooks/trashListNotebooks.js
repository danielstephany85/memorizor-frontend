import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NotebookListElement from './notebookListElement/notebookListElement.js';
import TrashNotebookModal from './trashNotbookModal/trashNotebookModal'

class TrashListNotebooks extends Component {

    constructor(props){
        super(props);
        this.state = {
            modalOpen: false,
            selectedNotebook: undefined,
            view: ''
        }
    }

    openModal = (notebook={}, view) => {
        this.setState({ 
            modalOpen: true,
            selectedNotebook: notebook,
            view: view
         });
    }
    closeModal = () => {
        this.setState({
            modalOpen: false,
            selectedNotebook: undefined
        });
    }

    filterNotebooks = () => {
        let notebooks;

        if (Array.isArray(this.props.notebooks)) {
            notebooks = this.props.notebooks.filter(book => book.in_trash);
            return notebooks.map(book => <NotebookListElement openModal={this.openModal} key={book._id} notebook={book} />);
        } else {
            return <span>Trash is empty</span>
        }
    }

    render = () => {
        return (
            <div className="note-list">
                <div className="note-list__row">
                    {this.filterNotebooks()}
                </div>
                <TrashNotebookModal modalOpen={this.state.modalOpen}
                                    closeModal={this.closeModal} 
                                    notebook={this.state.selectedNotebook} 
                                    view={this.state.view}
                                    restoreNotebook={this.props.restoreNotebook}
                                    deleteNotebook={this.props.deleteNotebook}
                                    />
            </div>
        );
    }
}

TrashListNotebooks.propTypes = {
    restoreNotebook: PropTypes.func.isRequired,
    deleteNotebook: PropTypes.func.isRequired,
    notebook: PropTypes.array,
}

export default TrashListNotebooks;