import React from 'react';
import PropTypes from 'prop-types';
import './notebookListElement.scss';
import { Link } from 'react-router-dom';

const NotebookListElement = (props) => {
    return(
        <Link to={`/trash/${props.notebook._id}`} className="notebook-list-element">
            <span className="notebook-list-element__title">{props.notebook.title}</span>
            <button type='button' onClick={(e) => { e.preventDefault(); props.openModal(props.notebook, 'restore') }}>restore</button>
            <button type='button' onClick={(e) => { e.preventDefault(); props.openModal(props.notebook, 'delete') }}>delete</button>
        </Link>
    );
}

NotebookListElement.propTypes = {
    notebook: PropTypes.object.isRequired,
    openModal: PropTypes.func.isRequired
}

export default NotebookListElement;