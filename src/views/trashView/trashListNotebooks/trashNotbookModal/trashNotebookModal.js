import React, {Component} from 'react';
import Modal from '../../../../components/modal/Modal.js';
import PropTypes from 'prop-types';
import DeleteView from './views/deleteView';
import RestoreView from './views/restoreView';


class TrashNotebookModal extends Component {

    handleRestoreNotebook = () => {
        this.props.restoreNotebook(this.props.notebook._id)
        .then(()=>{
            this.props.closeModal();
        });
    }

    handleDeleteNotebook = () => {
        this.props.deleteNotebook(this.props.notebook._id)
        .then(()=> {
            this.props.closeModal();
        });
    }
    
    render = () => {
        let notebookTitle = this.props.notebook ? this.props.notebook.title : '';

        return (
            <Modal modalOpen={this.props.modalOpen} closeModal={this.props.closeModal}>
                <div>
                    <button type='button' className="modal-close" onClick={this.props.closeModal}>close</button>
                    {this.props.view === 'delete' ? <DeleteView notebookTitle={notebookTitle} handleDeleteNotebook={this.handleDeleteNotebook}/> : undefined}
                    {this.props.view === 'restore' ? <RestoreView notebookTitle={notebookTitle} handleRestoreNotebook={this.handleRestoreNotebook}/> : undefined}
                </div>
            </Modal>
        )
    }
}

TrashNotebookModal.propTypes = {
    notebook: PropTypes.object,
    closeModal: PropTypes.func.isRequired,
    modalOpen: PropTypes.bool.isRequired,
    view: PropTypes.string
}

export default TrashNotebookModal;