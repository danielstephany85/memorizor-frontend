import React from 'react';
import PropTypes from 'prop-types';

const DeleteView = function (props) {
    return (
        <div>
            <p>Deleting <b>{props.notebookTitle}</b> will permentantly delete a notebook and all associated notes</p>
            <div className="modal-btn-container">
                <button type="button" className="main-btn" onClick={props.handleDeleteNotebook}>Delete Notebook</button>
            </div>
        </div>
    );
}

DeleteView.propTypes = {
    notebookTitle: PropTypes.string,
    handleDeleteNotebook: PropTypes.func.isRequired
}

export default DeleteView;