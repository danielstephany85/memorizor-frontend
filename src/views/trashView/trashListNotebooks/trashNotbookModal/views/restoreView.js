import React from 'react';
import PropTypes from 'prop-types';

const RestoreView = function (props) {
    return (
        <div>
            <p>Restoring <b>{props.notebookTitle}</b> will also restore all notes associated with this notebook</p>
            <div className="modal-btn-container">
                <button type="button" className="main-btn" onClick={props.handleRestoreNotebook}>Restore Notebook</button>
            </div>
        </div>
    );
}

RestoreView.propTypes = {
    notebookTitle: PropTypes.string,
    handleRestoreNotebook: PropTypes.func.isRequired
}

export default RestoreView;