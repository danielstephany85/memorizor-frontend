import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch, Link } from "react-router-dom";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { 
    setActiveNotebook,
    restoreNotebook,
    deleteNotebook } from '../../store/actions/user';
import { 
    fetchTrashNotes,
    deleteSelectedNotes,
    deleteNote,
    restoreSelectedNotes,
    restoreNote } from '../../store/actions/notes';
import TrashListNotes from './trashListNotes/trashListNotes';
import TrashListNotebooks from './trashListNotebooks/trashListNotebooks';
import ViewHeader from '@components/ViewHeader';
import ScrollContainer from '../../components/scrollContainer/scrollContainer.js';
import MultiSelectHeader from './multiSelectHeader/multiSelectHeader';
import RestoreNotesModal from './restoreNotesModal/restoreNotesModal';
import ButtonMain from '@components/ButtonMain';

class TrashView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            multiSelected: {
                length: 0,
                notes: []
            },
            mountHeader: false,
            modalOpen: false,
            activeNotebookId: undefined
        }

        this.moutHeaderTimer = null;
        this.setActiveNoteBook = bindActionCreators(setActiveNotebook, this.props.dispatch);
        this.restoreNotebook = bindActionCreators(restoreNotebook, this.props.dispatch);
        this.fetchTrashNotes = bindActionCreators(fetchTrashNotes, this.props.dispatch);
        this.deleteSelectedNotes = bindActionCreators(deleteSelectedNotes, this.props.dispatch);
        this.deleteNote = bindActionCreators(deleteNote, this.props.dispatch);
        this.restoreSelectedNotes = bindActionCreators(restoreSelectedNotes, this.props.dispatch);
        this.restoreNote = bindActionCreators(restoreNote, this.props.dispatch);
        this.deleteNotebook = bindActionCreators(deleteNotebook, this.props.dispatch);
    }

    
    
    componentDidMount = () => { this.fetchTrashNotes();}

    componentDidUpdate = () => { this.mountHeader(); }
    
    componentWillUnmount = () => {clearTimeout(this.moutHeaderTimer);}

    mountHeader = () => {
        if (this.state.multiSelected.length && !this.state.mountHeader) {
            this.setState({ mountHeader: true });
        } else if (!this.state.multiSelected.length && this.state.mountHeader) {
            // adding a deley to mountHeader to allow for animating out
            this.moutHeaderTimer = setTimeout(() => {
                this.setState({ mountHeader: false });
            }, 400);
        }
    }

    handleDeleteSelectedNotes = () => {
        const notesArray = [];
        this.state.multiSelected.notes.forEach(note => notesArray.push(note._id));
        this.deleteSelectedNotes(notesArray)
        .then((json)=>{
            this.setState({
                multiSelected: {
                    length: 0,
                    notes: []
                },
                selectedNote: undefined
            });
        });
    }

    restoreSelectedNotesCall = (note = undefined, notebook = undefined) => {
        return new Promise((resolve,reject)=>{
            let noteArray = [];

            if (note) {
                noteArray.push(note._id);
            } else {
                this.state.multiSelected.notes.forEach(note => {
                    noteArray.push(note._id);
                });
            }
                
            if (noteArray.length && notebook) {
                this.restoreSelectedNotes(noteArray, notebook)
                    .then((json) => {
                        this.setState({
                            multiSelected: {
                                length: 0,
                                notes: []
                            }
                        });
                        return resolve();
                    })
                    .catch((e)=>{
                        return reject(e);
                    });
            }else {
                console.log("ERROR: handleRestoreSelectedNotes expects \"noteArray\" and \"notebook\" ");
                return reject("ERROR: handleRestoreSelectedNotes expects \"noteArray\" and \"notebook\" ");        
            }
        });
    }

    addToMultiSelectList = (note) => {
        let notes = this.state.multiSelected.notes;
        notes.push(note)
        this.setState((state)=>({
            multiSelected: {
                length: (state.multiSelected.length + 1),
                notes:  notes
            }
        }));
    }

    removeFromMultiSelectList = (id) => {
        this.setState((state)=>({
            multiSelected: {
                length: (state.multiSelected.length - 1),
                notes: this.state.multiSelected.notes.filter(note=> note._id !== id)
            }
        }));
    }

    deselectAll = () => {
        this.setState({
            multiSelected: {
                length: 0,
                notes: []
            }
        }, () => {
            const multiselectbtns = Array.from(document.querySelectorAll(".multi-select-btn.active"));
            if (multiselectbtns) {
                multiselectbtns.forEach(btn => {
                    btn.classList.remove("active");
                })
            }
        });
    }

    openModal = (note = undefined) => {
        this.setState({ 
            modalOpen: true,
            selectedNote: note
        });
    }

    closeModal = () => {
        this.setState({ modalOpen: false });
    }

    render = () => {
        let trashLink;
        if (this.props.location.pathname === "/trash/notebooks") {
            trashLink = <ButtonMain className='outline-btn' to={`/trash`} component={Link}>notes view</ButtonMain>
        } else if (this.props.location.pathname === "/trash"){
            trashLink = <ButtonMain className='outline-btn' to={`/trash/notebooks`} component={Link}>notebooks view</ButtonMain>
        } else {
            trashLink = <ButtonMain className='outline-btn' to={`/trash`} component={Link}>all notes view</ButtonMain>
        }

        const headerItems = (
        <React.Fragment>
                <ViewHeader 
                    title={<h2>Trash</h2>} 
                    action={trashLink} 
                />
                {this.state.mountHeader ? 
                    <MultiSelectHeader 
                        multiSelected={this.state.multiSelected}
                        deselectAll={this.deselectAll}
                        handleDeleteSelectedNotes={this.handleDeleteSelectedNotes}
                        openModal={this.openModal}
                        /> 
                    : 
                    undefined}
        </React.Fragment>);

        return (
            <ScrollContainer header={headerItems} >
                <Switch>
                    <Route exact path={`${this.props.match.url}`} render={(props) => <TrashListNotes 
                        {...props}
                        notes={this.props.notes}
                        removeFromMultiSelectList={this.removeFromMultiSelectList}
                        addToMultiSelectList={this.addToMultiSelectList}
                        multiSelectActive={this.state.multiSelected.length ? true : false}
                        deleteNote={this.deleteNote}
                        restoreNote={this.restoreNote}
                        notebooks={this.props.notebooks}
                        openModal={this.openModal}
                    />} />
                    <Route exact path={`${this.props.match.url}/notebooks`} render={(props) => <TrashListNotebooks 
                        {...props}
                        notes={this.props.notes}
                        restoreNotebook={this.restoreNotebook}
                        deleteNotebook={this.deleteNotebook}
                        notebooks={this.props.notebooks}
                        multiSelected={this.props.multiSelected}
                    />} />
                    <Route exact path={`${this.props.match.url}/:notebookId`} render={(props) => <TrashListNotes 
                        {...props}
                        notes={this.props.notes}
                        removeFromMultiSelectList={this.removeFromMultiSelectList}
                        addToMultiSelectList={this.addToMultiSelectList}
                        multiSelectActive={this.state.multiSelected.length ? true : false}
                        deleteNote={this.deleteNote}
                        notebooks={this.props.notebooks}
                        openModal={this.openModal}
                    />} />
                </Switch>
                <RestoreNotesModal 
                    modalOpen={this.state.modalOpen}
                    closeModal={this.closeModal}
                    notebooks={this.props.notebooks}
                    selectedNote={this.state.selectedNote}
                    multiSelected={this.props.multiSelected}
                    restoreSelectedNotesCall={this.restoreSelectedNotesCall}
                />
            </ScrollContainer>
        );
    }
}

TrashView.propTypes = {
    notes: PropTypes.array,
    token: PropTypes.string.isRequired,
    multiSelectActive: PropTypes.bool,
}

const mapStateToProps = (state) => (
    {
        token: state.userReducer.token,
        notes: state.notesReducer.trashNotes,
        notebooks: state.userReducer.user.notebooks,
    }
);

export default connect(mapStateToProps)(TrashView);