import React from 'react';
import Modal from '../../../components/modal/Modal.js';
import RestoreNotesForm from './restoreNotesForm/restoreNotesForm';
import PropTypes from 'prop-types';


function RestoreNotesModal (props) {
        return (
            <Modal modalOpen={props.modalOpen} closeModal={props.closeModal}>
                <div>
                    <button type='button' className="modal-close" onClick={props.closeModal}>close</button>
                    <RestoreNotesForm 
                        closeModal={props.closeModal} 
                        notebooks={props.notebooks}
                        restoreSelectedNotesCall={props.restoreSelectedNotesCall}
                        selectedNote={props.selectedNote}
                    />
                </div>
            </Modal>
        );
}

RestoreNotesModal.propTypes = {
    notebooks: PropTypes.array,
    selectedNote: PropTypes.object,
    closeModal: PropTypes.func.isRequired,
    modalOpen: PropTypes.bool.isRequired,
    restoreSelectedNotesCall: PropTypes.func
}

export default RestoreNotesModal;