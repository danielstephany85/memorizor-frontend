import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './multiSelectHeader.scss';
import ButtonMain from '@components/ButtonMain';
import MoveToNotebook from '@components/icons/MoveToNotebook';
import MoveToTrash from '@components/icons/MoveToTrash';

class MultiSelectHeader extends Component {

    componentDidMount = () => {
        setTimeout(()=>{
            this.header.classList.add('animate-in');
        },50);
    }

    componentDidUpdate = (props) => {
        if (!this.props.multiSelected.length){
            this.header.classList.remove('animate-in');
        };
    }

    render = () => {
        return (
            <div className="multi-select-header" ref={(div)=>{this.header = div}}>
                <div className="multi-select-header__content" >   
                    <div className="multi-select-header__group">
                        <button className="deselect-btn" onClick={this.props.deselectAll}><i className="fas fa-times"></i></button>
                        <span>selected {this.props.multiSelected.length}</span>
                    </div>
                    <div className="multi-select-header__group">
                        <ButtonMain className="move-to-notebook-btn" iconEnd onClick={this.props.openMoveToNoteBookModal}><span>move to notebook</span><MoveToNotebook /></ButtonMain>
                        <ButtonMain className="place-in-trash-btn" iconEnd onClick={this.props.openTrashNotesModal}><span>place in trash</span><MoveToTrash /></ButtonMain>
                    </div>
                </div>
            </div>
        );
    }
}

MultiSelectHeader.propTypes = {
    multiSelected: PropTypes.object.isRequired,
    deselectAll: PropTypes.func.isRequired,
    openMoveToNoteBookModal: PropTypes.func
}

export default MultiSelectHeader;