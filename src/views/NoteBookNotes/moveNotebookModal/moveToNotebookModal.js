import React from 'react';
import Modal from '../../../components/modal/Modal.js';
import MoveToNotebookForm from './moveToNotebookForm/moveToNotebookForm.js';
import PropTypes from 'prop-types';


function MoveToNotebookModal (props) {
        return (
            <Modal modalOpen={props.modalOpen} closeModal={props.closeModal}>
                <div>
                    <button type='button' className="modal-close" onClick={props.closeMoveToNoteBookModal}>close</button>
                    <MoveToNotebookForm 
                        closeModal={props.closeModal}
                        notebooks={props.notebooks}
                        changeSelectedNotesNotebook={props.changeSelectedNotesNotebook}
                        selectedNotes={props.selectedNotes}
                    />
                </div>
            </Modal>
        );
}

MoveToNotebookModal.propTypes = {
    notebooks: PropTypes.array,
    selectedNotes: PropTypes.array,
    closeModal: PropTypes.func.isRequired,
    modalOpen: PropTypes.bool.isRequired,
    changeSelectedNotesNotebook: PropTypes.func
}

export default MoveToNotebookModal;