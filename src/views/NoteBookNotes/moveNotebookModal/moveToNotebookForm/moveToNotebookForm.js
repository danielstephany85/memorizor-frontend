import React, {Component} from 'react';
import PropTypes from 'prop-types';

class MoveToNotebookForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            selectedNotebook: "",
            selectedNotebookErr: undefined
        }
    }

    validateSelect = () => {
        let validBoolean = false;
        if(!this.state.selectedNotebook){
            this.setState({
                selectedNotebookErr: "please select a notebook"
            });
            validBoolean = false;
        }else {
            this.setState({
                selectedNotebookErr: undefined
            });
            validBoolean = true;
        }
        return validBoolean
    }

    changeSelectedNotesNotebook = () => {
        let notes = this.props.selectedNotes ? this.props.selectedNotes : []

        if (this.validateSelect()){
            this.props.changeSelectedNotesNotebook(notes, this.state.selectedNotebook)
            .then(() => {
                this.setState({
                    selectedNotebook: undefined
                }, () => {
                    this.props.closeModal();
                });
            });
        }
    }

    render = () => {
        let options = this.props.notebooks.filter((book)=> !book.in_trash).map(notebook => {
            return <option value={notebook._id} key={notebook._id}>{notebook.title}</option>
        });

        const select = <select
                            className={`${this.state.selectedNotebookErr ? "error" : ""}`}
                            value={this.state.selectedNotebook}
                            onChange={(e) => {
                                this.setState({ selectedNotebook: e.target.value }, () => {
                                    this.validateSelect();
                                });
                            }}
                            onBlur={this.validateSelect}
                        >
                            <option value="">select a notbook</option>
                            {options};
                        </select>

        return (
            <form noValidate>
                <p>Select a notebook to move your notes into.</p>
                <div className={`form-item ${this.state.selectedNotebookErr ? 'error' : ''}`}>
                    {select}<br/>
                    {this.state.selectedNotebookErr ? <span className="error-msg">{this.state.selectedNotebookErr}</span> : ''}
                </div>
                <div className="modal-btn-container">
                    <button type="button" className="main-btn" onClick={this.changeSelectedNotesNotebook}>place in new notebook</button>
                </div>
            </form>
        );
    }
}

MoveToNotebookForm.propTypes = {
    notebooks: PropTypes.array.isRequired,
    selectedNotes: PropTypes.array,
    closeModal: PropTypes.func.isRequired,
    changeSelectedNotesNotebook: PropTypes.func.isRequired
}

export default MoveToNotebookForm;