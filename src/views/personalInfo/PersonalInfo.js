import React, {Component} from 'react';
import ScrollContainer from '../../components/scrollContainer/scrollContainer.js';
import ViewHeader from '@components/ViewHeader';
import Input from '../../components/input/Input.js';
import Label from '../../components/label/Label.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateUser} from '../../store/actions/user';
import './PersonalInfo.scss';


class PersonalInfo extends Component {

    constructor(props){
        super(props);
        this.state = {
            name: this.props.user.name,
            nameErr: '',
            email: this.props.user.email,
            emailErr: ''
        }
        this.name = this.props.user.name;
        this.email = this.props.user.email;
        this.updateUser = bindActionCreators(updateUser, this.props.dispatch);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const data = {
            name: this.state.name,
            email: this.state.email
        }

        this.name = this.state.name;
        this.eamil = this.state.email;

        this.updateUser(data);
    }

    updateValue = (key) => {
        return (e) => this.setState({[key]: e.target.value});
    }

    displayUpdateButton = () => {
        let button = null;
        if ( this.name !== this.state.name || this.email !== this.state.email ){
            button = <button className='form-btn' type="submit">update</button>;
        }
        return button;
    }

    render () {
        return (
            <ScrollContainer className="personal-info" header={<ViewHeader title={<h2>Personal info</h2>} />} >
                <section className="content-section">
                    <div className="inner-content inner-content--no-mt">
                        <div className="flat-content">
                            <form onSubmit={this.handleSubmit}>
                                <div className="form-content">
                                    <div className={`form-item ${this.state.nameErr ? 'error' : ''}`}>
                                        <Label htmlFor="password">User Name</Label>
                                        <Input borderBottom type="text" name="password" value={this.state.name} onChange={this.updateValue("name")} />
                                        {this.state.nameErr ? <span className="error-msg">{this.state.nameErr}</span> : ''}
                                    </div>
                                    <div className={`form-item ${this.state.emailErr ? 'error' : ''}`}>
                                        <Label htmlFor="email">Email</Label>
                                        <Input borderBottom type="email" id="email" name="email" value={this.state.email} onChange={this.updateValue("email")} />
                                        {this.state.emailErr ? <span className="error-msg">{this.state.emailErr}</span> : ''}
                                    </div>
                                    {this.displayUpdateButton()}
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </ScrollContainer>
        );
    }
} 

const mapStateToProps = (state) => (
    {
        user: state.userReducer.user
    }
);

export default connect(mapStateToProps)(PersonalInfo);