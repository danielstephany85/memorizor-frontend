import React, {Component} from 'react';
import {connect} from 'react-redux';
import NotebookForm from './NotebookForm';
import TrashNotebookModal from './TrashNotebookModal';
import { bindActionCreators } from 'redux';
import { addNotebookToTrash} from '../../store/actions/user';
import ViewHeader from '@components/ViewHeader';
import ScrollContainer from '../../components/scrollContainer/scrollContainer.js';
import NotebookLink from '@components/NotebookLink';
import LoadingBar from '@components/LoadingBar';
import './notebookView.scss';


class NotebookView extends Component {

    constructor(props){
        super(props);
        this.state = {
            modalOpen: false,
            loading: false
        }
        this.addNotebookToTrash = bindActionCreators(addNotebookToTrash, this.props.dispatch);
    }

    setLoading = (isLoading) => {
        this.setState({ loading: isLoading})
    }

    openModal = (e, notebook = {}) => {
        e.preventDefault();
        this.setState({
            selectedNotebook: notebook,
            modalOpen: true,
        });
    }

    closeModal = () => {
        this.setState({
            modalOpen: false,
        });
    }

    createNoteBookLinks = () => {
        let links = this.props.user.notebooks.filter(book => !book.in_trash);
        return links.map(book => <NotebookLink key={book._id} to={`/notebooks-notes/${book._id}`} iconAction={(e) => { this.openModal(e, book) }}>{book.title}</NotebookLink>);
    }


    render = () => {
        let links = this.createNoteBookLinks();
        let userHasNotebooks = !!this.props.user.notebooks.length;

        let body = (
            <React.Fragment>
                <NotebookForm setLoading={this.setLoading} loading={this.state.loading}/>
                <div className='notebook-list'>
                    <h3>Notebooks</h3>
                    {userHasNotebooks ? links : 'currently no notebooks'}
                </div>
                <TrashNotebookModal closeModal={this.closeModal} modalOpen={this.state.modalOpen} notebook={this.state.selectedNotebook} addNotebookToTrash={this.addNotebookToTrash} />
            </React.Fragment>
        );


        return (
            <ScrollContainer className="notebooks" header={<ViewHeader title={<h2>Notebooks</h2>} />} >
                {this.state.loading ? <LoadingBar /> : null}
                {body}
            </ScrollContainer>  
        );
    }
}

const mapStateToProps = (state) => ({user: state.userReducer.user })

export default connect(mapStateToProps)(NotebookView);