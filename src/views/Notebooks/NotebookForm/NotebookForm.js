import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './NotebookForm.scss';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {addNotebook} from '@store/actions/user';
import ButtonMain from '@components/ButtonMain'

class NotebookForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            title: '',
            titleErr: ''
        }
        this.addNotebook = bindActionCreators(addNotebook, this.props.dispatch)
    }

    handleAddNotebook = (e) => {
        e.preventDefault();
        if (this.state.title) {
            this.props.setLoading(true)
            this.addNotebook(this.state.title)
            .then(() => {
                this.setState({
                    title: '',
                    titleErr: '',
                });
                this.props.setLoading(false)
            })
            .catch(err => {
                if (err.data && err.data.message) {
                    this.setState({ titleErr: err.data.message });
                }
                this.props.setLoading(false)
            });
        } else {
            this.setState({ titleErr: 'a notebook name is required' });
        }
    }

    render = () => {
        return(
            <div className='notebook-form'>
                <h3>Create a new notebook</h3>
                <form onSubmit={this.handleAddNotebook}>
                    <input type="text" placeholder="notebook title..." value={this.state.title} onChange={(e)=>{this.setState({title: e.target.value})}}/>
                    <ButtonMain type='submit' bgColor="primary" loading={this.props.loading}>+</ButtonMain>
                </form>
                {this.state.titleErr ? <span className="error-msg">{this.state.titleErr}</span>:undefined}
            </div>
        );
    }
}

NotebookForm.propTypes = {
    setLoading: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({})

export default connect(mapStateToProps)(NotebookForm);