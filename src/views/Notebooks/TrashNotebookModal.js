import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from '../../components/modal/Modal';
import ButtonMain from '@components/ButtonMain';

class TrashNotebookModal extends Component {
    constructor(props){
        super(props);
        this.state = {loading: false}
    }

    handleAddNotebookToTrash = (notebookId) => {
        if (this.props.notebook && !this.state.loading){
            this.setState({ loading: true });
            this.props.addNotebookToTrash(this.props.notebook._id)
                .then(() => {
                    this.setState({loading: false});
                    this.props.closeModal();
                });
        }
    }

    render = () => {
        let notebookTitle = this.props.notebook ? this.props.notebook.title : undefined;

        return (
            <Modal modalOpen={this.props.modalOpen} closeModal={this.props.closeModal}>
                <div>
                    <button className="modal-close" onClick={this.props.closeModal}>close</button>
                    <p>Are you sure you would like to delete the <b>{notebookTitle}</b> notebook. This will place all associated notes in the trash.</p>
                    <div className="modal-btn-container">
                        <ButtonMain type="button" size="lg" bgColor="primary" loading={this.state.loading} onClick={this.handleAddNotebookToTrash}>delete</ButtonMain>
                    </div>
                </div>
            </Modal>
        );
    }
}

TrashNotebookModal.propTypes = {
    titel: PropTypes.string,
    id: PropTypes.string,
    addNotebookToTrash: PropTypes.func
}

export default TrashNotebookModal;