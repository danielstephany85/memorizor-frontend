import React from 'react';
import ViewHeader from '@components/ViewHeader';
import ScrollContainer from '../../components/scrollContainer/scrollContainer.js';
import List from '../../components/list/List.js';
import ListLink from '../../components/listItemLink/ListItemLink.js';
import ListItem from '../../components/listItem/listItem.js';
import Hr from '../../components/hr/Hr.js';

const SettingsView = function(props){

    return(
        <ScrollContainer header={<ViewHeader title={<h2>Settings</h2>} />}>
            <List>
                <ListLink link to="/personal-info">View or Edit Personal info</ListLink>
                <ListLink link to="/password-change">Change Password</ListLink>
            </List>
            <Hr />
            <List>
                <ListItem secondary button onClick={props.logOut}>logout</ListItem>
            </List>
        </ScrollContainer>
    );
}

export default SettingsView;