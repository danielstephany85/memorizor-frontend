import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loginUser } from "@store/actions/user";
import './LoginView.scss';
import BgRepeated from '@components/BgRepeated';
import ButtonMain from '@components/ButtonMain';

class LoginView extends Component {

    constructor(props){
        super(props);
        this.state = {
            email:'',
            password: '',
            errors: undefined,
            loading: false
        }
        this.loginUser = bindActionCreators(loginUser, this.props.dispatch);
    }

    hasValidInputs = () => {
        let errData = {};
        if (!this.state.email) {
            errData.emailErr = "Email is required"
        }
        if (!this.state.email.includes("@") || !this.state.email.includes(".")) {
            errData.emailErr = "not a valid email"
        }
        if (!this.state.password) {
            errData.passwordErr = 'password is required';
        }
        if (Object.keys(errData).length) {
            this.setState({ ...errData });
            return false;
        }
        return true;
    }

    handleFetch = () => {
        const data = {
            email: this.state.email,
            password: this.state.password
        }
        this.setState({ loading: true });
        this.loginUser(data)
        .then(json => {
            localStorage.setItem('token', json.data.token);
            this.props.history.push('/notebooks');
        })
        .catch(json => {
            this.setState({
                passwordErr: json.data.message,
                loading: false
            });
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            emailErr: '',
            passwordErr: '',
        });

        if (this.hasValidInputs()) {
            this.handleFetch();
        }
    }

    render = () => {
        return(
            <section className="login-page content-section">
                <BgRepeated bgColor="blue" />
                <div className="inner-content">
                    <div className="form-card">
                        <h2>Welcome back please log in</h2>
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-content">
                                <div className={`form-item ${this.state.emailErr ? 'error' : ''}`}>
                                    <label htmlFor="email">Email</label>
                                    <input type="text" id="email" name="email" value={this.state.email} onChange={(e) => { this.setState({ email: e.target.value }) }} autoFocus/>
                                    {this.state.emailErr ? <span className="error-msg">{this.state.emailErr}</span> : ''}
                                </div>
                                <div className={`form-item ${this.state.passwordErr ? 'error' : ''}`}>
                                    <label htmlFor="password">Password</label>
                                    <input type="password" id="password" name="password" value={this.state.password} onChange={(e) => { this.setState({ password: e.target.value }) }} />
                                    {this.state.passwordErr ? <span className="error-msg">{this.state.passwordErr}</span> : ''}
                                </div>
                                <ButtonMain type="submit" bgColor="secondary" size="lg" loading={this.state.loading}>login</ButtonMain>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
           
        );
    }
}

export default connect()(LoginView);