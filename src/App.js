import React, { Component } from 'react';
import { HashRouter, Route} from "react-router-dom";
import { createBrowserHistory } from "history"
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MainHeader from './components/mainHeader/mainHeader';
import { logOut, setUser } from './store/actions/user';
import { setNotes, removeOutDatedTrash } from './store/actions/notes';
import RouteTracker from '@components/_utils/RouteTracker';
import ScrollContainer from '@components/scrollContainer/scrollContainer';
import ViewHeader from '@components/ViewHeader';
import LeftMainNav from '@components/leftMainNav/leftMainNav';

import LoggedOutRoutes from './routes/LoggedOutRoutes';
import LoggedInRoutes from './routes/LoggedInRoutes';

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      canNavigateBack: false,
      loadingUser: false
    }
    this.logOut = bindActionCreators(logOut, this.props.dispatch);
    this.setUser = bindActionCreators(setUser, this.props.dispatch);
    this.setNotes = bindActionCreators(setNotes, this.props.dispatch);
    this.history = createBrowserHistory();
    //routeTracker gets passed history and is used to check if the site can navigate back
    this.routeTracker = new RouteTracker(this.history);
  }

  componentDidMount = () => {
    const token = localStorage.getItem('token');
    if (token){
      this.setState({loadingUser: true});
      this.setUser(token)
      .then(() => {
        this.setState({ loadingUser: false }, () => {
          // this.setNotes();
          removeOutDatedTrash(token);
        });
      });
    }
    this.logRoute();
  }

  logRoute = () => {
    this.routeTracker.checkAndLogStepForward();
    if ((this.routeTracker.locationList.length - 1) && !this.state.canNavigateBack) {
      this.setState({ canNavigateBack: true });
    } else if (!(this.routeTracker.locationList.length - 1) && this.state.canNavigateBack){
      this.setState({ canNavigateBack: false });
    }
  }

  handleLogout = () => {
    this.logOut();
    localStorage.removeItem('token');
  }

  render() {
    const leftMainNav = <Route path="/" render={(props) => <LeftMainNav {...props} signedIn={this.props.signedIn} activeNotebook={this.props.activeNotebook} stepBack={this.routeTracker.stepBack} canNavigateBack={this.state.canNavigateBack} />} />
    let routes = <ScrollContainer header={<ViewHeader title={<h2>Loading...</h2>} />} body={"loading..."} />

    if (!this.state.loadingUser){
      routes = this.props.signedIn ? <LoggedInRoutes logRoute={this.logRoute} activeNotebook={this.props.activeNotebook} logOut={this.handleLogout} /> : <LoggedOutRoutes logRoute={this.logRoute} />;
    }

    return (
      <HashRouter>
        <div className="site-constraint">
          <Route path="/" render={(props) => <MainHeader {...props} logOut={this.logOut} signedIn={this.props.signedIn}/>} />
          <main className="main-content">
            {this.props.signedIn || this.state.loadingUser ? leftMainNav : null}
            <div className="right-main-content">{routes}</div>
          </main>
        </div>
      </HashRouter>
    );
  }
}

const mapStateToProps = (state) => (
  {
    signedIn: state.userReducer.signedIn,
    activeNotebook: state.userReducer.activeNotebook
  }
);

export default connect(mapStateToProps)(App);
