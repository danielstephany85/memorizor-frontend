import React from 'react';
import { Switch, Redirect } from "react-router-dom";
import LoggerRoute from '@components/loggerRoute';
//views
import NewNoteView from '@views/newNoteVeiw';
import NoteView from '@views/noteView';
import Notebooks from '@views/Notebooks';
import TrashView from '@views/trashView/trashView';
import SettingsView from '@views/settingsView/settingsView.js';
import PersonalInfo from '@views/personalInfo/PersonalInfo.js';
import PasswordChange from '@views/passwordChange/PasswordChange.js';
import NotebookNotes from '@views/NotebookNotes';

export default function LoggedInRoutes({logOut, activeNotebook, logRoute}) {
    return (
        <Switch>
            <LoggerRoute exact path="/create-note" logRoute={logRoute} render={props => activeNotebook ? <NewNoteView {...props} /> : <Redirect to="/notebooks" />} />
            <LoggerRoute exact path="/note/:noteId" logRoute={logRoute} render={props => activeNotebook ? <NoteView {...props} /> : <Redirect to="/notebooks" />} />
            <LoggerRoute exact path="/notebooks" logRoute={logRoute} render={props => <Notebooks {...props} />} />
            <LoggerRoute exact path="/notebooks-notes/:notebookId" logRoute={logRoute} render={props => <NotebookNotes {...props} />} />
            <LoggerRoute path="/trash" logRoute={logRoute} render={props => <TrashView {...props} />} />
            <LoggerRoute path="/settings" logRoute={logRoute} render={props => <SettingsView {...props} logOut={logOut} />} />
            <LoggerRoute path="/personal-info" logRoute={logRoute} render={props => <PersonalInfo {...props} />} />
            <LoggerRoute path="/password-change" logRoute={logRoute} render={props => <PasswordChange {...props} />} />
            <LoggerRoute path="/" logRoute={logRoute} render={props => <Redirect to="/notebooks" />} />
        </Switch>
    );
}