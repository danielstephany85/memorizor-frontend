import React from 'react';
import PropTypes from "prop-types";
import { Switch } from "react-router-dom";
import LoggerRoute from '@components/loggerRoute';

import Home from '@views/Home';
import SignUp from '@views/SignUp';
import LoginView from '@views/LoginView';

export default function LoggedOutRoutes({ logRoute }){

    return (
        <Switch>
            <LoggerRoute exact path="/login" logRoute={logRoute} render={props => <LoginView {...props} />} />
            <LoggerRoute exact path="/create-account" logRoute={logRoute} render={props => <SignUp {...props} />} />
            <LoggerRoute path="/" logRoute={logRoute} render={props => <Home {...props} />} />
        </Switch>
    );
}

LoggedOutRoutes.propTypes = {
    logRoute: PropTypes.func.isRequired
}